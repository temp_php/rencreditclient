<?php

namespace Rencredit;

use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use Http\Adapter\Guzzle6\Client;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Phpro\SoapClient\Event\Subscriber\LogSubscriber;
use Phpro\SoapClient\Middleware\WsaMiddleware;
use Phpro\SoapClient\Middleware\WsseMiddleware;
use Phpro\SoapClient\Soap\Driver\ExtSoap\ExtSoapEngineFactory;
use Phpro\SoapClient\Soap\Driver\ExtSoap\ExtSoapOptions;
use Phpro\SoapClient\Soap\Handler\HttPlugHandle;
use Symfony\Component\EventDispatcher\EventDispatcher;
use GuzzleHttp\Client as GuzzleClient;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

class RencreditClientFactory
{

    public static function factory(string $wsdl): \Rencredit\RencreditClient
    {
        $log = new Logger('rencreditClientLogger');
        $log->pushHandler(new StreamHandler('./path/to/your.log', Logger::DEBUG));

        $config = [
            'timeout' => 5,
//            'debug' => true
//            'handler' => $stack
            // ...
        ];
        $adapter = GuzzleAdapter::createWithConfig($config);
        $handler = HttPlugHandle::createForClient($adapter);
        $wsse = new WsseMiddleware('private_key.pem', 'cert.pem');
        $handler->addMiddleware($wsse);
        $handler->addMiddleware(new WsaMiddleware());
        $engine = ExtSoapEngineFactory::fromOptionsWithHandler(
                ExtSoapOptions::defaults($wsdl, ['location' => 'https://fastloanpreprodft.rencredit.ru/SecLoanServicePilot/SecLoanCrbOnlineSrv'])
                // ExtSoapOptions::defaults($wsdl, ['location' => 'http://localhost:8088/mockportBinding'])
                ->withClassMap(RencreditClassmap::getCollection()),
            $handler
        );
        $eventDispatcher = new EventDispatcher();

        // https://github.com/phpro/soap-client/blob/master/docs/event-subscribers/logger.md
        $eventDispatcher->addSubscriber(new LogSubscriber($log));

        return new RencreditClient($engine, $eventDispatcher);
    }

}
