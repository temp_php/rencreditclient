<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\ResultInterface;

class ScoreResponse implements ResultInterface
{

    /**
     * @var string
     */
    private $correlationId;

    /**
     * @return string
     */
    public function getCorrelationId()
    {
        return $this->correlationId;
    }

    /**
     * @param string $correlationId
     * @return ScoreResponse
     */
    public function withCorrelationId($correlationId)
    {
        $new = clone $this;
        $new->correlationId = $correlationId;

        return $new;
    }


}

