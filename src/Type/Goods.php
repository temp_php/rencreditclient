<?php

namespace Rencredit\Type;

class Goods
{

    /**
     * @var int
     */
    private $category;

    /**
     * @var int
     */
    private $group;

    /**
     * @var string
     */
    private $vin;

    /**
     * @var string
     */
    private $entityName;

    /**
     * @var string
     */
    private $entityINN;

    /**
     * @var string
     */
    private $bic;

    /**
     * @var string
     */
    private $corAccount;

    /**
     * @var string
     */
    private $operAccount;

    /**
     * @var string
     */
    private $bankName;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $unitPriceInRub;

    /**
     * @var int
     */
    private $unitsNumber;

    /**
     * @param int $category
     */
    public function setCategory(int $category): void
    {
        $this->category = $category;
    }

    /**
     * @param int $group
     */
    public function setGroup(int $group): void
    {
        $this->group = $group;
    }

    /**
     * @param string $vin
     */
    public function setVin(string $vin): void
    {
        $this->vin = $vin;
    }

    /**
     * @param string $entityName
     */
    public function setEntityName(string $entityName): void
    {
        $this->entityName = $entityName;
    }

    /**
     * @param string $entityINN
     */
    public function setEntityINN(string $entityINN): void
    {
        $this->entityINN = $entityINN;
    }

    /**
     * @param string $bic
     */
    public function setBic(string $bic): void
    {
        $this->bic = $bic;
    }

    /**
     * @param string $corAccount
     */
    public function setCorAccount(string $corAccount): void
    {
        $this->corAccount = $corAccount;
    }

    /**
     * @param string $operAccount
     */
    public function setOperAccount(string $operAccount): void
    {
        $this->operAccount = $operAccount;
    }

    /**
     * @param string $bankName
     */
    public function setBankName(string $bankName): void
    {
        $this->bankName = $bankName;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param float $unitPriceInRub
     */
    public function setUnitPriceInRub(float $unitPriceInRub): void
    {
        $this->unitPriceInRub = $unitPriceInRub;
    }

    /**
     * @param int $unitsNumber
     */
    public function setUnitsNumber(int $unitsNumber): void
    {
        $this->unitsNumber = $unitsNumber;
    }

    /**
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param int $category
     * @return Goods
     */
    public function withCategory($category)
    {
        $new = clone $this;
        $new->category = $category;

        return $new;
    }

    /**
     * @return int
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param int $group
     * @return Goods
     */
    public function withGroup($group)
    {
        $new = clone $this;
        $new->group = $group;

        return $new;
    }

    /**
     * @return string
     */
    public function getVin()
    {
        return $this->vin;
    }

    /**
     * @param string $vin
     * @return Goods
     */
    public function withVin($vin)
    {
        $new = clone $this;
        $new->vin = $vin;

        return $new;
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * @param string $entityName
     * @return Goods
     */
    public function withEntityName($entityName)
    {
        $new = clone $this;
        $new->entityName = $entityName;

        return $new;
    }

    /**
     * @return string
     */
    public function getEntityINN()
    {
        return $this->entityINN;
    }

    /**
     * @param string $entityINN
     * @return Goods
     */
    public function withEntityINN($entityINN)
    {
        $new = clone $this;
        $new->entityINN = $entityINN;

        return $new;
    }

    /**
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * @param string $bic
     * @return Goods
     */
    public function withBic($bic)
    {
        $new = clone $this;
        $new->bic = $bic;

        return $new;
    }

    /**
     * @return string
     */
    public function getCorAccount()
    {
        return $this->corAccount;
    }

    /**
     * @param string $corAccount
     * @return Goods
     */
    public function withCorAccount($corAccount)
    {
        $new = clone $this;
        $new->corAccount = $corAccount;

        return $new;
    }

    /**
     * @return string
     */
    public function getOperAccount()
    {
        return $this->operAccount;
    }

    /**
     * @param string $operAccount
     * @return Goods
     */
    public function withOperAccount($operAccount)
    {
        $new = clone $this;
        $new->operAccount = $operAccount;

        return $new;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     * @return Goods
     */
    public function withBankName($bankName)
    {
        $new = clone $this;
        $new->bankName = $bankName;

        return $new;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Goods
     */
    public function withName($name)
    {
        $new = clone $this;
        $new->name = $name;

        return $new;
    }

    /**
     * @return float
     */
    public function getUnitPriceInRub()
    {
        return $this->unitPriceInRub;
    }

    /**
     * @param float $unitPriceInRub
     * @return Goods
     */
    public function withUnitPriceInRub($unitPriceInRub)
    {
        $new = clone $this;
        $new->unitPriceInRub = $unitPriceInRub;

        return $new;
    }

    /**
     * @return int
     */
    public function getUnitsNumber()
    {
        return $this->unitsNumber;
    }

    /**
     * @param int $unitsNumber
     * @return Goods
     */
    public function withUnitsNumber($unitsNumber)
    {
        $new = clone $this;
        $new->unitsNumber = $unitsNumber;

        return $new;
    }


}

