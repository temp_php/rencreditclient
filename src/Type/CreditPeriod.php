<?php

namespace Rencredit\Type;

class CreditPeriod
{

    /**
     * @var int
     */
    private $creditPeriod;

    /**
     * @return int
     */
    public function getCreditPeriod()
    {
        return $this->creditPeriod;
    }

    /**
     * @param int $creditPeriod
     * @return CreditPeriod
     */
    public function withCreditPeriod($creditPeriod)
    {
        $new = clone $this;
        $new->creditPeriod = $creditPeriod;

        return $new;
    }


}

