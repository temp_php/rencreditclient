<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\RequestInterface;

class CheckScoreStatusRequest implements RequestInterface
{

    /**
     * @var string
     */
    private $correlationId;

    /**
     * Constructor
     *
     * @var string $correlationId
     */
    public function __construct($correlationId)
    {
        $this->correlationId = $correlationId;
    }

    /**
     * @return string
     */
    public function getCorrelationId()
    {
        return $this->correlationId;
    }

    /**
     * @param string $correlationId
     * @return CheckScoreStatusRequest
     */
    public function withCorrelationId($correlationId)
    {
        $new = clone $this;
        $new->correlationId = $correlationId;

        return $new;
    }


}

