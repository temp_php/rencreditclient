<?php

namespace Rencredit\Type;

class Document
{

    /**
     * @var string
     */
    private $documentNumber;

    /**
     * @var string
     */
    private $documentSerialNumber;

    /**
     * @var int
     */
    private $documentType;

    /**
     * @var string
     */
    private $issueDate;

    /**
     * @var string
     */
    private $issuedBy;

    /**
     * @var string
     */
    private $issueBranchCode;

    /**
     * @return string
     */
    public function getDocumentNumber()
    {
        return $this->documentNumber;
    }

    /**
     * @param string $documentNumber
     * @return Document
     */
    public function withDocumentNumber($documentNumber)
    {
        $new = clone $this;
        $new->documentNumber = $documentNumber;

        return $new;
    }

    /**
     * @return string
     */
    public function getDocumentSerialNumber()
    {
        return $this->documentSerialNumber;
    }

    /**
     * @param string $documentSerialNumber
     * @return Document
     */
    public function withDocumentSerialNumber($documentSerialNumber)
    {
        $new = clone $this;
        $new->documentSerialNumber = $documentSerialNumber;

        return $new;
    }

    /**
     * @return int
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * @param int $documentType
     * @return Document
     */
    public function withDocumentType($documentType)
    {
        $new = clone $this;
        $new->documentType = $documentType;

        return $new;
    }

    /**
     * @return string
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }

    /**
     * @param string $issueDate
     * @return Document
     */
    public function withIssueDate($issueDate)
    {
        $new = clone $this;
        $new->issueDate = $issueDate;

        return $new;
    }

    /**
     * @return string
     */
    public function getIssuedBy()
    {
        return $this->issuedBy;
    }

    /**
     * @param string $issuedBy
     * @return Document
     */
    public function withIssuedBy($issuedBy)
    {
        $new = clone $this;
        $new->issuedBy = $issuedBy;

        return $new;
    }

    /**
     * @return string
     */
    public function getIssueBranchCode()
    {
        return $this->issueBranchCode;
    }

    /**
     * @param string $issueBranchCode
     * @return Document
     */
    public function withIssueBranchCode($issueBranchCode)
    {
        $new = clone $this;
        $new->issueBranchCode = $issueBranchCode;

        return $new;
    }


}

