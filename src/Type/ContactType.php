<?php

namespace Rencredit\Type;

class ContactType
{

    /**
     * @var int
     */
    private $preferredContactType;

    /**
     * @var int
     */
    private $preferredContactTimeFrom;

    /**
     * @var int
     */
    private $preferredContactTimeTo;

    /**
     * @var int
     */
    private $reportDeliveryAddress;

    /**
     * @var bool
     */
    private $reportDeliveryType;

    /**
     * @return int
     */
    public function getPreferredContactType()
    {
        return $this->preferredContactType;
    }

    /**
     * @param int $preferredContactType
     * @return ContactType
     */
    public function withPreferredContactType($preferredContactType)
    {
        $new = clone $this;
        $new->preferredContactType = $preferredContactType;

        return $new;
    }

    /**
     * @return int
     */
    public function getPreferredContactTimeFrom()
    {
        return $this->preferredContactTimeFrom;
    }

    /**
     * @param int $preferredContactTimeFrom
     * @return ContactType
     */
    public function withPreferredContactTimeFrom($preferredContactTimeFrom)
    {
        $new = clone $this;
        $new->preferredContactTimeFrom = $preferredContactTimeFrom;

        return $new;
    }

    /**
     * @return int
     */
    public function getPreferredContactTimeTo()
    {
        return $this->preferredContactTimeTo;
    }

    /**
     * @param int $preferredContactTimeTo
     * @return ContactType
     */
    public function withPreferredContactTimeTo($preferredContactTimeTo)
    {
        $new = clone $this;
        $new->preferredContactTimeTo = $preferredContactTimeTo;

        return $new;
    }

    /**
     * @return int
     */
    public function getReportDeliveryAddress()
    {
        return $this->reportDeliveryAddress;
    }

    /**
     * @param int $reportDeliveryAddress
     * @return ContactType
     */
    public function withReportDeliveryAddress($reportDeliveryAddress)
    {
        $new = clone $this;
        $new->reportDeliveryAddress = $reportDeliveryAddress;

        return $new;
    }

    /**
     * @return bool
     */
    public function getReportDeliveryType()
    {
        return $this->reportDeliveryType;
    }

    /**
     * @param bool $reportDeliveryType
     * @return ContactType
     */
    public function withReportDeliveryType($reportDeliveryType)
    {
        $new = clone $this;
        $new->reportDeliveryType = $reportDeliveryType;

        return $new;
    }


}

