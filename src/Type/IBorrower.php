<?php

namespace Rencredit\Type;

class IBorrower extends Person
{

    /**
     * @var string
     */
    private $birthDate;

    /**
     * @var string
     */
    private $passportSerialNumber;

    /**
     * @var string
     */
    private $passportNumber;

    /**
     * @var string
     */
    private $passportIssueDate;

    /**
     * @var string
     */
    private $snils;

    /**
     * @var \Rencredit\Type\Phone
     */
    private $mobilePhone;

    /**
     * @var float
     */
    private $monthlyIncomeInRub;

    /**
     * @var \Rencredit\Type\ConsentFlag
     */
    private $consentFlags;

    /**
     * @return string
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param string $birthDate
     * @return IBorrower
     */
    public function withBirthDate($birthDate)
    {
        $new = clone $this;
        $new->birthDate = $birthDate;

        return $new;
    }

    /**
     * @return string
     */
    public function getPassportSerialNumber()
    {
        return $this->passportSerialNumber;
    }

    /**
     * @param string $passportSerialNumber
     * @return IBorrower
     */
    public function withPassportSerialNumber($passportSerialNumber)
    {
        $new = clone $this;
        $new->passportSerialNumber = $passportSerialNumber;

        return $new;
    }

    /**
     * @return string
     */
    public function getPassportNumber()
    {
        return $this->passportNumber;
    }

    /**
     * @param string $passportNumber
     * @return IBorrower
     */
    public function withPassportNumber($passportNumber)
    {
        $new = clone $this;
        $new->passportNumber = $passportNumber;

        return $new;
    }

    /**
     * @return string
     */
    public function getPassportIssueDate()
    {
        return $this->passportIssueDate;
    }

    /**
     * @param string $passportIssueDate
     * @return IBorrower
     */
    public function withPassportIssueDate($passportIssueDate)
    {
        $new = clone $this;
        $new->passportIssueDate = $passportIssueDate;

        return $new;
    }

    /**
     * @return string
     */
    public function getSnils()
    {
        return $this->snils;
    }

    /**
     * @param string $snils
     * @return IBorrower
     */
    public function withSnils($snils)
    {
        $new = clone $this;
        $new->snils = $snils;

        return $new;
    }

    /**
     * @return \Rencredit\Type\Phone
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * @param \Rencredit\Type\Phone $mobilePhone
     * @return IBorrower
     */
    public function withMobilePhone($mobilePhone)
    {
        $new = clone $this;
        $new->mobilePhone = $mobilePhone;

        return $new;
    }

    /**
     * @return float
     */
    public function getMonthlyIncomeInRub()
    {
        return $this->monthlyIncomeInRub;
    }

    /**
     * @param float $monthlyIncomeInRub
     * @return IBorrower
     */
    public function withMonthlyIncomeInRub($monthlyIncomeInRub)
    {
        $new = clone $this;
        $new->monthlyIncomeInRub = $monthlyIncomeInRub;

        return $new;
    }

    /**
     * @return \Rencredit\Type\ConsentFlag
     */
    public function getConsentFlags()
    {
        return $this->consentFlags;
    }

    /**
     * @param \Rencredit\Type\ConsentFlag $consentFlags
     * @return IBorrower
     */
    public function withConsentFlags($consentFlags)
    {
        $new = clone $this;
        $new->consentFlags = $consentFlags;

        return $new;
    }


}

