<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\ResultInterface;

class ConfirmOfferResponse implements ResultInterface
{

    /**
     * @var bool
     */
    private $return;

    /**
     * @var string
     */
    private $message;

    /**
     * @return bool
     */
    public function getReturn()
    {
        return $this->return;
    }

    /**
     * @param bool $return
     * @return ConfirmOfferResponse
     */
    public function withReturn($return)
    {
        $new = clone $this;
        $new->return = $return;

        return $new;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return ConfirmOfferResponse
     */
    public function withMessage($message)
    {
        $new = clone $this;
        $new->message = $message;

        return $new;
    }


}

