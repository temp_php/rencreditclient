<?php

namespace Rencredit\Type;

class BinaryFile
{

    /**
     * @var string
     */
    private $body;

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     * @return BinaryFile
     */
    public function withBody($body)
    {
        $new = clone $this;
        $new->body = $body;

        return $new;
    }


}

