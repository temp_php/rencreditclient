<?php

namespace Rencredit\Type;

class Insurance
{

    /**
     * @var int
     */
    private $insuranceCode;

    /**
     * @var float
     */
    private $insuranceSum;

    /**
     * @var int
     */
    private $insurerId;

    /**
     * @return int
     */
    public function getInsuranceCode()
    {
        return $this->insuranceCode;
    }

    /**
     * @param int $insuranceCode
     * @return Insurance
     */
    public function withInsuranceCode($insuranceCode)
    {
        $new = clone $this;
        $new->insuranceCode = $insuranceCode;

        return $new;
    }

    /**
     * @return float
     */
    public function getInsuranceSum()
    {
        return $this->insuranceSum;
    }

    /**
     * @param float $insuranceSum
     * @return Insurance
     */
    public function withInsuranceSum($insuranceSum)
    {
        $new = clone $this;
        $new->insuranceSum = $insuranceSum;

        return $new;
    }

    /**
     * @return int
     */
    public function getInsurerId()
    {
        return $this->insurerId;
    }

    /**
     * @param int $insurerId
     * @return Insurance
     */
    public function withInsurerId($insurerId)
    {
        $new = clone $this;
        $new->insurerId = $insurerId;

        return $new;
    }


}

