<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\ResultInterface;

class CheckStatusScansResponse implements ResultInterface
{

    /**
     * @var \Rencredit\Type\ScanStatusesType
     */
    private $return;

    /**
     * @return \Rencredit\Type\ScanStatusesType
     */
    public function getReturn()
    {
        return $this->return;
    }

    /**
     * @param \Rencredit\Type\ScanStatusesType $return
     * @return CheckStatusScansResponse
     */
    public function withReturn($return)
    {
        $new = clone $this;
        $new->return = $return;

        return $new;
    }


}

