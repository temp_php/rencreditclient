<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\ResultInterface;

class GenerateDocsResponse implements ResultInterface
{

    /**
     * @var \Rencredit\Type\GenerateDocsResult
     */
    private $return;

    /**
     * @return \Rencredit\Type\GenerateDocsResult
     */
    public function getReturn()
    {
        return $this->return;
    }

    /**
     * @param \Rencredit\Type\GenerateDocsResult $return
     * @return GenerateDocsResponse
     */
    public function withReturn($return)
    {
        $new = clone $this;
        $new->return = $return;

        return $new;
    }


}

