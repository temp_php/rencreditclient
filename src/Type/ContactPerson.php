<?php

namespace Rencredit\Type;

class ContactPerson extends Person
{

    /**
     * @var \Rencredit\Type\Phone
     */
    private $mobilePhone;

    /**
     * @var \Rencredit\Type\Phone
     */
    private $homePhone;

    /**
     * @var \Rencredit\Type\Phone
     */
    private $workPhone;

    /**
     * @var string
     */
    private $email;

    /**
     * @var float
     */
    private $monthlyIncomeInRub;

    /**
     * @var int
     */
    private $relation;

    /**
     * @var \Rencredit\Type\StationaryPhone
     */
    private $registrationPhone;

    /**
     * @return Phone
     */
    public function getMobilePhone(): Phone
    {
        return $this->mobilePhone;
    }

    /**
     * @param Phone $mobilePhone
     */
    public function setMobilePhone(Phone $mobilePhone): void
    {
        $this->mobilePhone = $mobilePhone;
    }

    /**
     * @return Phone
     */
    public function getHomePhone(): Phone
    {
        return $this->homePhone;
    }

    /**
     * @param Phone $homePhone
     */
    public function setHomePhone(Phone $homePhone): void
    {
        $this->homePhone = $homePhone;
    }

    /**
     * @return Phone
     */
    public function getWorkPhone(): Phone
    {
        return $this->workPhone;
    }

    /**
     * @param Phone $workPhone
     */
    public function setWorkPhone(Phone $workPhone): void
    {
        $this->workPhone = $workPhone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return float
     */
    public function getMonthlyIncomeInRub(): float
    {
        return $this->monthlyIncomeInRub;
    }

    /**
     * @param float $monthlyIncomeInRub
     */
    public function setMonthlyIncomeInRub(float $monthlyIncomeInRub): void
    {
        $this->monthlyIncomeInRub = $monthlyIncomeInRub;
    }

    /**
     * @return int
     */
    public function getRelation(): int
    {
        return $this->relation;
    }

    /**
     * @param int $relation
     */
    public function setRelation(int $relation): void
    {
        $this->relation = $relation;
    }

    /**
     * @return StationaryPhone
     */
    public function getRegistrationPhone(): StationaryPhone
    {
        return $this->registrationPhone;
    }

    /**
     * @param StationaryPhone $registrationPhone
     */
    public function setRegistrationPhone(StationaryPhone $registrationPhone): void
    {
        $this->registrationPhone = $registrationPhone;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getPatronymic(): string
    {
        return $this->patronymic;
    }

    /**
     * @param string $patronymic
     */
    public function setPatronymic(string $patronymic): void
    {
        $this->patronymic = $patronymic;
    }
}
