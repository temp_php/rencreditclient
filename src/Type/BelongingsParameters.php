<?php

namespace Rencredit\Type;

class BelongingsParameters
{

    /**
     * @var int
     */
    private $belongingsType;

    /**
     * @var float
     */
    private $cost;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var bool
     */
    private $mortgage;

    /**
     * @return int
     */
    public function getBelongingsType()
    {
        return $this->belongingsType;
    }

    /**
     * @param int $belongingsType
     * @return BelongingsParameters
     */
    public function withBelongingsType($belongingsType)
    {
        $new = clone $this;
        $new->belongingsType = $belongingsType;

        return $new;
    }

    /**
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param float $cost
     * @return BelongingsParameters
     */
    public function withCost($cost)
    {
        $new = clone $this;
        $new->cost = $cost;

        return $new;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return BelongingsParameters
     */
    public function withComment($comment)
    {
        $new = clone $this;
        $new->comment = $comment;

        return $new;
    }

    /**
     * @return bool
     */
    public function getMortgage()
    {
        return $this->mortgage;
    }

    /**
     * @param bool $mortgage
     * @return BelongingsParameters
     */
    public function withMortgage($mortgage)
    {
        $new = clone $this;
        $new->mortgage = $mortgage;

        return $new;
    }


}

