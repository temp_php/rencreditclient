<?php

namespace Rencredit\Type;

class LoanException
{

    /**
     * @var string
     */
    private $faultReason;

    /**
     * @var string
     */
    private $message;

    /**
     * @return string
     */
    public function getFaultReason()
    {
        return $this->faultReason;
    }

    /**
     * @param string $faultReason
     * @return LoanException
     */
    public function withFaultReason($faultReason)
    {
        $new = clone $this;
        $new->faultReason = $faultReason;

        return $new;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return LoanException
     */
    public function withMessage($message)
    {
        $new = clone $this;
        $new->message = $message;

        return $new;
    }


}

