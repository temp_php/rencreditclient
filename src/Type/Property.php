<?php

namespace Rencredit\Type;

class Property
{

    /**
     * @var \Rencredit\Type\Car
     */
    private $car;

    /**
     * @var \Rencredit\Type\Address
     */
    private $realEstate;

    /**
     * Property constructor.
     * @param Car $car
     */
    public function __construct(Car $car)
    {
        $this->car = $car;
    }

    /**
     * @return \Rencredit\Type\Car
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * @param \Rencredit\Type\Car $car
     * @return Property
     */
    public function withCar($car)
    {
        $new = clone $this;
        $new->car = $car;

        return $new;
    }

    /**
     * @return \Rencredit\Type\Address
     */
    public function getRealEstate()
    {
        return $this->realEstate;
    }

    /**
     * @param \Rencredit\Type\Address $realEstate
     * @return Property
     */
    public function withRealEstate($realEstate)
    {
        $new = clone $this;
        $new->realEstate = $realEstate;

        return $new;
    }


}

