<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\ResultInterface;

class ProductInfoResponse implements ResultInterface
{

    /**
     * @var int
     */
    private $code;

    /**
     * @var \Rencredit\Type\ProductInformation
     */
    private $productInformation;

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return ProductInfoResponse
     */
    public function withCode($code)
    {
        $new = clone $this;
        $new->code = $code;

        return $new;
    }

    /**
     * @return \Rencredit\Type\ProductInformation
     */
    public function getProductInformation()
    {
        return $this->productInformation;
    }

    /**
     * @param \Rencredit\Type\ProductInformation $productInformation
     * @return ProductInfoResponse
     */
    public function withProductInformation($productInformation)
    {
        $new = clone $this;
        $new->productInformation = $productInformation;

        return $new;
    }


}

