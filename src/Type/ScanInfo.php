<?php

namespace Rencredit\Type;

class ScanInfo
{

    /**
     * @var int
     */
    private $scanDocType;

    /**
     * @var int
     */
    private $scanStatus;

    /**
     * @var string
     */
    private $scanErrorMessage;

    /**
     * @return int
     */
    public function getScanDocType()
    {
        return $this->scanDocType;
    }

    /**
     * @param int $scanDocType
     * @return ScanInfo
     */
    public function withScanDocType($scanDocType)
    {
        $new = clone $this;
        $new->scanDocType = $scanDocType;

        return $new;
    }

    /**
     * @return int
     */
    public function getScanStatus()
    {
        return $this->scanStatus;
    }

    /**
     * @param int $scanStatus
     * @return ScanInfo
     */
    public function withScanStatus($scanStatus)
    {
        $new = clone $this;
        $new->scanStatus = $scanStatus;

        return $new;
    }

    /**
     * @return string
     */
    public function getScanErrorMessage()
    {
        return $this->scanErrorMessage;
    }

    /**
     * @param string $scanErrorMessage
     * @return ScanInfo
     */
    public function withScanErrorMessage($scanErrorMessage)
    {
        $new = clone $this;
        $new->scanErrorMessage = $scanErrorMessage;

        return $new;
    }


}

