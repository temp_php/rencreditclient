<?php

namespace Rencredit\Type;

class Address
{

    /**
     * @var string
     */
    private $apartmentNumber;

    /**
     * @var string
     */
    private $house;

    /**
     * @var string
     */
    private $building;

    /**
     * @var string
     */
    private $district;

    /**
     * @var string
     */
    private $structure;

    /**
     * @var int
     */
    private $placeType;

    /**
     * @var string
     */
    private $placeName;

    /**
     * @var int
     */
    private $propertyStatus;

    /**
     * @var string
     */
    private $regDate;

    /**
     * @var int
     */
    private $region;

    /**
     * @var string
     */
    private $street;

    /**
     * @var mixed
     */
    private $streetType;

    /**
     * @var int
     */
    private $timeOfLivingInMonths;

    /**
     * @var string
     */
    private $town;

    /**
     * @var int
     */
    private $townType;

    /**
     * @var string
     */
    private $zipCode;

    /**
     * @var int
     */
    private $country;

    /**
     * @var string
     */
    private $area;

    /**
     * @var int
     */
    private $cityType;

    /**
     * @var string
     */
    private $cityName;

    /**
     * @var string
     */
    private $flat;

    /**
     * @param string $apartmentNumber
     */
    public function setApartmentNumber(string $apartmentNumber): void
    {
        $this->apartmentNumber = $apartmentNumber;
    }

    /**
     * @param string $house
     */
    public function setHouse(string $house): void
    {
        $this->house = $house;
    }

    /**
     * @param string $building
     */
    public function setBuilding(string $building): void
    {
        $this->building = $building;
    }

    /**
     * @param string $district
     */
    public function setDistrict(string $district): void
    {
        $this->district = $district;
    }

    /**
     * @param string $structure
     */
    public function setStructure(string $structure): void
    {
        $this->structure = $structure;
    }

    /**
     * @param int $placeType
     */
    public function setPlaceType(int $placeType): void
    {
        $this->placeType = $placeType;
    }

    /**
     * @param string $placeName
     */
    public function setPlaceName(string $placeName): void
    {
        $this->placeName = $placeName;
    }

    /**
     * @param int $propertyStatus
     */
    public function setPropertyStatus(int $propertyStatus): void
    {
        $this->propertyStatus = $propertyStatus;
    }

    /**
     * @param string $regDate
     */
    public function setRegDate(string $regDate): void
    {
        $this->regDate = $regDate;
    }

    /**
     * @param int $region
     */
    public function setRegion(int $region): void
    {
        $this->region = $region;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @param mixed $streetType
     */
    public function setStreetType($streetType): void
    {
        $this->streetType = $streetType;
    }

    /**
     * @param int $timeOfLivingInMonths
     */
    public function setTimeOfLivingInMonths(int $timeOfLivingInMonths): void
    {
        $this->timeOfLivingInMonths = $timeOfLivingInMonths;
    }

    /**
     * @param string $town
     */
    public function setTown(string $town): void
    {
        $this->town = $town;
    }

    /**
     * @param int $townType
     */
    public function setTownType(int $townType): void
    {
        $this->townType = $townType;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode(string $zipCode): void
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @param int $country
     */
    public function setCountry(int $country): void
    {
        $this->country = $country;
    }

    /**
     * @param string $area
     */
    public function setArea(string $area): void
    {
        $this->area = $area;
    }

    /**
     * @param int $cityType
     */
    public function setCityType(int $cityType): void
    {
        $this->cityType = $cityType;
    }

    /**
     * @param string $cityName
     */
    public function setCityName(string $cityName): void
    {
        $this->cityName = $cityName;
    }

    /**
     * @param string $flat
     */
    public function setFlat(string $flat): void
    {
        $this->flat = $flat;
    }

    /**
     * @return string
     */
    public function getApartmentNumber()
    {
        return $this->apartmentNumber;
    }

    /**
     * @param string $apartmentNumber
     * @return Address
     */
    public function withApartmentNumber($apartmentNumber)
    {
        $new = clone $this;
        $new->apartmentNumber = $apartmentNumber;

        return $new;
    }

    /**
     * @return string
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * @param string $house
     * @return Address
     */
    public function withHouse($house)
    {
        $new = clone $this;
        $new->house = $house;

        return $new;
    }

    /**
     * @return string
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * @param string $building
     * @return Address
     */
    public function withBuilding($building)
    {
        $new = clone $this;
        $new->building = $building;

        return $new;
    }

    /**
     * @return string
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param string $district
     * @return Address
     */
    public function withDistrict($district)
    {
        $new = clone $this;
        $new->district = $district;

        return $new;
    }

    /**
     * @return string
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * @param string $structure
     * @return Address
     */
    public function withStructure($structure)
    {
        $new = clone $this;
        $new->structure = $structure;

        return $new;
    }

    /**
     * @return int
     */
    public function getPlaceType()
    {
        return $this->placeType;
    }

    /**
     * @param int $placeType
     * @return Address
     */
    public function withPlaceType($placeType)
    {
        $new = clone $this;
        $new->placeType = $placeType;

        return $new;
    }

    /**
     * @return string
     */
    public function getPlaceName()
    {
        return $this->placeName;
    }

    /**
     * @param string $placeName
     * @return Address
     */
    public function withPlaceName($placeName)
    {
        $new = clone $this;
        $new->placeName = $placeName;

        return $new;
    }

    /**
     * @return int
     */
    public function getPropertyStatus()
    {
        return $this->propertyStatus;
    }

    /**
     * @param int $propertyStatus
     * @return Address
     */
    public function withPropertyStatus($propertyStatus)
    {
        $new = clone $this;
        $new->propertyStatus = $propertyStatus;

        return $new;
    }

    /**
     * @return string
     */
    public function getRegDate()
    {
        return $this->regDate;
    }

    /**
     * @param string $regDate
     * @return Address
     */
    public function withRegDate($regDate)
    {
        $new = clone $this;
        $new->regDate = $regDate;

        return $new;
    }

    /**
     * @return int
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param int $region
     * @return Address
     */
    public function withRegion($region)
    {
        $new = clone $this;
        $new->region = $region;

        return $new;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return Address
     */
    public function withStreet($street)
    {
        $new = clone $this;
        $new->street = $street;

        return $new;
    }

    /**
     * @return mixed
     */
    public function getStreetType()
    {
        return $this->streetType;
    }

    /**
     * @param mixed $streetType
     * @return Address
     */
    public function withStreetType($streetType)
    {
        $new = clone $this;
        $new->streetType = $streetType;

        return $new;
    }

    /**
     * @return int
     */
    public function getTimeOfLivingInMonths()
    {
        return $this->timeOfLivingInMonths;
    }

    /**
     * @param int $timeOfLivingInMonths
     * @return Address
     */
    public function withTimeOfLivingInMonths($timeOfLivingInMonths)
    {
        $new = clone $this;
        $new->timeOfLivingInMonths = $timeOfLivingInMonths;

        return $new;
    }

    /**
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * @param string $town
     * @return Address
     */
    public function withTown($town)
    {
        $new = clone $this;
        $new->town = $town;

        return $new;
    }

    /**
     * @return int
     */
    public function getTownType()
    {
        return $this->townType;
    }

    /**
     * @param int $townType
     * @return Address
     */
    public function withTownType($townType)
    {
        $new = clone $this;
        $new->townType = $townType;

        return $new;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     * @return Address
     */
    public function withZipCode($zipCode)
    {
        $new = clone $this;
        $new->zipCode = $zipCode;

        return $new;
    }

    /**
     * @return int
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param int $country
     * @return Address
     */
    public function withCountry($country)
    {
        $new = clone $this;
        $new->country = $country;

        return $new;
    }

    /**
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param string $area
     * @return Address
     */
    public function withArea($area)
    {
        $new = clone $this;
        $new->area = $area;

        return $new;
    }

    /**
     * @return int
     */
    public function getCityType()
    {
        return $this->cityType;
    }

    /**
     * @param int $cityType
     * @return Address
     */
    public function withCityType($cityType)
    {
        $new = clone $this;
        $new->cityType = $cityType;

        return $new;
    }

    /**
     * @return string
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * @param string $cityName
     * @return Address
     */
    public function withCityName($cityName)
    {
        $new = clone $this;
        $new->cityName = $cityName;

        return $new;
    }

    /**
     * @return string
     */
    public function getFlat()
    {
        return $this->flat;
    }

    /**
     * @param string $flat
     * @return Address
     */
    public function withFlat($flat)
    {
        $new = clone $this;
        $new->flat = $flat;

        return $new;
    }


}

