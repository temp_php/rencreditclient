<?php

namespace Rencredit\Type;

class Phone
{

    /**
     * @var string
     */
    private $area;

    /**
     * @var string
     */
    private $extension;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var string
     */
    private $registeredOn;

    /**
     * Phone constructor.
     * @param string $area
     * @param string $extension
     * @param string $number
     * @param string $phoneNumber
     * @param string $registeredOn
     */
    public function __construct(string $area, string $extension, string $number, string $phoneNumber, string $registeredOn)
    {
        $this->area = $area;
        $this->extension = $extension;
        $this->number = $number;
        $this->phoneNumber = $phoneNumber;
        $this->registeredOn = $registeredOn;
    }

    /**
     * @return string
     */
    public function getArea(): string
    {
        return $this->area;
    }

    /**
     * @param string $area
     */
    public function setArea(string $area): void
    {
        $this->area = $area;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     */
    public function setExtension(string $extension): void
    {
        $this->extension = $extension;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getRegisteredOn(): string
    {
        return $this->registeredOn;
    }

    /**
     * @param string $registeredOn
     */
    public function setRegisteredOn(string $registeredOn): void
    {
        $this->registeredOn = $registeredOn;
    }
}
