<?php

namespace Rencredit\Type;

class Chattels
{

    /**
     * @var \Rencredit\Type\BelongingsParameters
     */
    private $parameters;

    /**
     * @return \Rencredit\Type\BelongingsParameters
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param \Rencredit\Type\BelongingsParameters $parameters
     * @return Chattels
     */
    public function withParameters($parameters)
    {
        $new = clone $this;
        $new->parameters = $parameters;

        return $new;
    }


}

