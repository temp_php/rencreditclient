<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\ResultInterface;

class CancelResponse implements ResultInterface
{

    /**
     * @var bool
     */
    private $response;

    /**
     * @return bool
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param bool $response
     * @return CancelResponse
     */
    public function withResponse($response)
    {
        $new = clone $this;
        $new->response = $response;

        return $new;
    }


}

