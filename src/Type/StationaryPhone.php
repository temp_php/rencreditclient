<?php

namespace Rencredit\Type;

class StationaryPhone
{

    /**
     * @var string
     */
    private $extension;

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     * @return StationaryPhone
     */
    public function withExtension($extension)
    {
        $new = clone $this;
        $new->extension = $extension;

        return $new;
    }


}

