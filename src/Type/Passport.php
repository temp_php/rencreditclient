<?php

namespace Rencredit\Type;

class Passport
{

    /**
     * @var string
     */
    private $documentNumber;

    /**
     * @var string
     */
    private $documentSerialNumber;

    /**
     * @var int
     */
    private $documentType;

    /**
     * @var \DateTimeInterface
     */
    private $issueDate;

    /**
     * @var string
     */
    private $issuedBy;

    /**
     * @var string
     */
    private $issueBranchCode;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $series;

    /**
     * @var string
     */
    private $issueOrg;

    /**
     * Passport constructor.
     * @param string $documentNumber
     * @param string $documentSerialNumber
     * @param int $documentType
     * @param \DateTimeInterface $issueDate
     * @param string $issuedBy
     * @param string $issueBranchCode
     */
    public function __construct(string $documentNumber, string $documentSerialNumber, int $documentType, \DateTimeInterface $issueDate, string $issuedBy, string $issueBranchCode)
    {
        $this->documentNumber = $documentNumber;
        $this->documentSerialNumber = $documentSerialNumber;
        $this->documentType = $documentType;
        $this->issueDate = $issueDate;
        $this->issuedBy = $issuedBy;
        $this->issueBranchCode = $issueBranchCode;
    }


    /**
     * @return string
     */
    public function getDocumentNumber()
    {
        return $this->documentNumber;
    }

    /**
     * @param string $documentNumber
     * @return Passport
     */
    public function withDocumentNumber($documentNumber)
    {
        $new = clone $this;
        $new->documentNumber = $documentNumber;

        return $new;
    }

    /**
     * @return string
     */
    public function getDocumentSerialNumber()
    {
        return $this->documentSerialNumber;
    }

    /**
     * @param string $documentSerialNumber
     * @return Passport
     */
    public function withDocumentSerialNumber($documentSerialNumber)
    {
        $new = clone $this;
        $new->documentSerialNumber = $documentSerialNumber;

        return $new;
    }

    /**
     * @return int
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * @param int $documentType
     * @return Passport
     */
    public function withDocumentType($documentType)
    {
        $new = clone $this;
        $new->documentType = $documentType;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }

    /**
     * @param \DateTimeInterface $issueDate
     * @return Passport
     */
    public function withIssueDate($issueDate)
    {
        $new = clone $this;
        $new->issueDate = $issueDate;

        return $new;
    }

    /**
     * @return string
     */
    public function getIssuedBy()
    {
        return $this->issuedBy;
    }

    /**
     * @param string $issuedBy
     * @return Passport
     */
    public function withIssuedBy($issuedBy)
    {
        $new = clone $this;
        $new->issuedBy = $issuedBy;

        return $new;
    }

    /**
     * @return string
     */
    public function getIssueBranchCode()
    {
        return $this->issueBranchCode;
    }

    /**
     * @param string $issueBranchCode
     * @return Passport
     */
    public function withIssueBranchCode($issueBranchCode)
    {
        $new = clone $this;
        $new->issueBranchCode = $issueBranchCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Passport
     */
    public function withNumber($number)
    {
        $new = clone $this;
        $new->number = $number;

        return $new;
    }

    /**
     * @return string
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * @param string $series
     * @return Passport
     */
    public function withSeries($series)
    {
        $new = clone $this;
        $new->series = $series;

        return $new;
    }

    /**
     * @return string
     */
    public function getIssueOrg()
    {
        return $this->issueOrg;
    }

    /**
     * @param string $issueOrg
     * @return Passport
     */
    public function withIssueOrg($issueOrg)
    {
        $new = clone $this;
        $new->issueOrg = $issueOrg;

        return $new;
    }


}

