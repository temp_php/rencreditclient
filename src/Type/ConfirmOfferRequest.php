<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\RequestInterface;

class ConfirmOfferRequest implements RequestInterface
{

    /**
     * @var string
     */
    private $correlationId;

    /**
     * @var bool
     */
    private $changePackages;

    /**
     * @var \Rencredit\Type\Offer
     */
    private $offer;

    /**
     * @var int
     */
    private $servicePackageId;

    /**
     * @var \Rencredit\Type\Insurance
     */
    private $insurance;

    /**
     * Constructor
     *
     * @var string $correlationId
     * @var bool $changePackages
     * @var \Rencredit\Type\Offer $offer
     * @var int $servicePackageId
     * @var \Rencredit\Type\Insurance $insurance
     */
    public function __construct($correlationId, $changePackages, $offer, $servicePackageId, $insurance)
    {
        $this->correlationId = $correlationId;
        $this->changePackages = $changePackages;
        $this->offer = $offer;
        $this->servicePackageId = $servicePackageId;
        $this->insurance = $insurance;
    }

    /**
     * @return string
     */
    public function getCorrelationId()
    {
        return $this->correlationId;
    }

    /**
     * @param string $correlationId
     * @return ConfirmOfferRequest
     */
    public function withCorrelationId($correlationId)
    {
        $new = clone $this;
        $new->correlationId = $correlationId;

        return $new;
    }

    /**
     * @return bool
     */
    public function getChangePackages()
    {
        return $this->changePackages;
    }

    /**
     * @param bool $changePackages
     * @return ConfirmOfferRequest
     */
    public function withChangePackages($changePackages)
    {
        $new = clone $this;
        $new->changePackages = $changePackages;

        return $new;
    }

    /**
     * @return \Rencredit\Type\Offer
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param \Rencredit\Type\Offer $offer
     * @return ConfirmOfferRequest
     */
    public function withOffer($offer)
    {
        $new = clone $this;
        $new->offer = $offer;

        return $new;
    }

    /**
     * @return int
     */
    public function getServicePackageId()
    {
        return $this->servicePackageId;
    }

    /**
     * @param int $servicePackageId
     * @return ConfirmOfferRequest
     */
    public function withServicePackageId($servicePackageId)
    {
        $new = clone $this;
        $new->servicePackageId = $servicePackageId;

        return $new;
    }

    /**
     * @return \Rencredit\Type\Insurance
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * @param \Rencredit\Type\Insurance $insurance
     * @return ConfirmOfferRequest
     */
    public function withInsurance($insurance)
    {
        $new = clone $this;
        $new->insurance = $insurance;

        return $new;
    }


}

