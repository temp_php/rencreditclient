<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\ResultInterface;

class AdditionalScoreResponse implements ResultInterface
{

    /**
     * @var int
     */
    private $code;

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return AdditionalScoreResponse
     */
    public function withCode($code)
    {
        $new = clone $this;
        $new->code = $code;

        return $new;
    }


}

