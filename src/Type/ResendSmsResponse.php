<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\ResultInterface;

class ResendSmsResponse implements ResultInterface
{

    /**
     * @var int
     */
    private $resultCode;

    /**
     * @return int
     */
    public function getResultCode()
    {
        return $this->resultCode;
    }

    /**
     * @param int $resultCode
     * @return ResendSmsResponse
     */
    public function withResultCode($resultCode)
    {
        $new = clone $this;
        $new->resultCode = $resultCode;

        return $new;
    }


}

