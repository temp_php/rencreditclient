<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\RequestInterface;

class GenerateDocsRequest implements RequestInterface
{

    /**
     * @var string
     */
    private $correlationId;

    /**
     * @var int
     */
    private $docSignType;

    /**
     * Constructor
     *
     * @var string $correlationId
     * @var int $docSignType
     */
    public function __construct($correlationId, $docSignType)
    {
        $this->correlationId = $correlationId;
        $this->docSignType = $docSignType;
    }

    /**
     * @return string
     */
    public function getCorrelationId()
    {
        return $this->correlationId;
    }

    /**
     * @param string $correlationId
     * @return GenerateDocsRequest
     */
    public function withCorrelationId($correlationId)
    {
        $new = clone $this;
        $new->correlationId = $correlationId;

        return $new;
    }

    /**
     * @return int
     */
    public function getDocSignType()
    {
        return $this->docSignType;
    }

    /**
     * @param int $docSignType
     * @return GenerateDocsRequest
     */
    public function withDocSignType($docSignType)
    {
        $new = clone $this;
        $new->docSignType = $docSignType;

        return $new;
    }


}

