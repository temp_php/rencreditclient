<?php

namespace Rencredit\Type;

class Offer
{

    /**
     * @var int
     */
    private $offerId;

    /**
     * @var float
     */
    private $creditLimit;

    /**
     * @var float
     */
    private $percentageRate;

    /**
     * @var \Rencredit\Type\DownPayment
     */
    private $downPayments;

    /**
     * @var \Rencredit\Type\CreditPeriod
     */
    private $creditPeriods;

    /**
     * @var string
     */
    private $productName;

    /**
     * @var float
     */
    private $plannedMonthlyPayment;

    /**
     * @var int
     */
    private $proofIncome;

    /**
     * @return int
     */
    public function getOfferId()
    {
        return $this->offerId;
    }

    /**
     * @param int $offerId
     * @return Offer
     */
    public function withOfferId($offerId)
    {
        $new = clone $this;
        $new->offerId = $offerId;

        return $new;
    }

    /**
     * @return float
     */
    public function getCreditLimit()
    {
        return $this->creditLimit;
    }

    /**
     * @param float $creditLimit
     * @return Offer
     */
    public function withCreditLimit($creditLimit)
    {
        $new = clone $this;
        $new->creditLimit = $creditLimit;

        return $new;
    }

    /**
     * @return float
     */
    public function getPercentageRate()
    {
        return $this->percentageRate;
    }

    /**
     * @param float $percentageRate
     * @return Offer
     */
    public function withPercentageRate($percentageRate)
    {
        $new = clone $this;
        $new->percentageRate = $percentageRate;

        return $new;
    }

    /**
     * @return \Rencredit\Type\DownPayment
     */
    public function getDownPayments()
    {
        return $this->downPayments;
    }

    /**
     * @param \Rencredit\Type\DownPayment $downPayments
     * @return Offer
     */
    public function withDownPayments($downPayments)
    {
        $new = clone $this;
        $new->downPayments = $downPayments;

        return $new;
    }

    /**
     * @return \Rencredit\Type\CreditPeriod
     */
    public function getCreditPeriods()
    {
        return $this->creditPeriods;
    }

    /**
     * @param \Rencredit\Type\CreditPeriod $creditPeriods
     * @return Offer
     */
    public function withCreditPeriods($creditPeriods)
    {
        $new = clone $this;
        $new->creditPeriods = $creditPeriods;

        return $new;
    }

    /**
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param string $productName
     * @return Offer
     */
    public function withProductName($productName)
    {
        $new = clone $this;
        $new->productName = $productName;

        return $new;
    }

    /**
     * @return float
     */
    public function getPlannedMonthlyPayment()
    {
        return $this->plannedMonthlyPayment;
    }

    /**
     * @param float $plannedMonthlyPayment
     * @return Offer
     */
    public function withPlannedMonthlyPayment($plannedMonthlyPayment)
    {
        $new = clone $this;
        $new->plannedMonthlyPayment = $plannedMonthlyPayment;

        return $new;
    }

    /**
     * @return int
     */
    public function getProofIncome()
    {
        return $this->proofIncome;
    }

    /**
     * @param int $proofIncome
     * @return Offer
     */
    public function withProofIncome($proofIncome)
    {
        $new = clone $this;
        $new->proofIncome = $proofIncome;

        return $new;
    }


}

