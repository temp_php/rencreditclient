<?php

namespace Rencredit\Type;

class MobilePhone extends Phone
{

    /**
     * @var string
     */
    private $registeredOn;

    /**
     * @return string
     */
    public function getRegisteredOn()
    {
        return $this->registeredOn;
    }

    /**
     * @param string $registeredOn
     * @return MobilePhone
     */
    public function withRegisteredOn($registeredOn)
    {
        $new = clone $this;
        $new->registeredOn = $registeredOn;

        return $new;
    }


}

