<?php

namespace Rencredit\Type;

class Form extends BasicForm
{

    /**
     * @var \Rencredit\Type\Borrower
     */
    private $borrower;

    /**
     * @var \Rencredit\Type\ContactPerson
     */
    private $coBorrower1;

    /**
     * @var \Rencredit\Type\ContactPerson
     */
    private $coBorrower2;

    /**
     * @var \Rencredit\Type\ContactPerson
     */
    private $contactPerson1;

    /**
     * @var \Rencredit\Type\ContactPerson
     */
    private $contactPerson2;

    /**
     * @var \Rencredit\Type\Goods
     */
    private $goods;

    /**
     * @var \Rencredit\Type\Insurance
     */
    private $insurances;

    /**
     * @var int
     */
    private $servicePackageId;

    /**
     * @var \Rencredit\Type\Scan
     */
    private $borrowerDocumentsScans;

    /**
     * @var bool
     */
    private $transferClaimDenied;

    /**
     * @var int
     */
    private $needCallCheck;

    /**
     * @var bool
     */
    private $autoReject;

    /**
     * @return int
     */
    public function getPosId(): int
    {
        return $this->posId;
    }

    /**
     * @param int $posId
     */
    public function setPosId(int $posId): void
    {
        $this->posId = $posId;
    }

    /**
     * @return string
     */
    public function getWebShopLink(): string
    {
        return $this->webShopLink;
    }

    /**
     * @param string $webShopLink
     */
    public function setWebShopLink(string $webShopLink): void
    {
        $this->webShopLink = $webShopLink;
    }

    /**
     * @return string
     */
    public function getUserLogin(): string
    {
        return $this->userLogin;
    }

    /**
     * @param string $userLogin
     */
    public function setUserLogin(string $userLogin): void
    {
        $this->userLogin = $userLogin;
    }

    /**
     * @return string
     */
    public function getPartnerID(): string
    {
        return $this->partnerID;
    }

    /**
     * @param string $partnerID
     */
    public function setPartnerID(string $partnerID): void
    {
        $this->partnerID = $partnerID;
    }

    /**
     * @return string
     */
    public function getPartnerNetId(): string
    {
        return $this->partnerNetId;
    }

    /**
     * @param string $partnerNetId
     */
    public function setPartnerNetId(string $partnerNetId): void
    {
        $this->partnerNetId = $partnerNetId;
    }

    /**
     * @return int
     */
    public function getCreditPeriodInMonths(): int
    {
        return $this->creditPeriodInMonths;
    }

    /**
     * @param int $creditPeriodInMonths
     */
    public function setCreditPeriodInMonths(int $creditPeriodInMonths): void
    {
        $this->creditPeriodInMonths = $creditPeriodInMonths;
    }

    /**
     * @return float
     */
    public function getInitialPayment(): float
    {
        return $this->initialPayment;
    }

    /**
     * @param float $initialPayment
     */
    public function setInitialPayment(float $initialPayment): void
    {
        $this->initialPayment = $initialPayment;
    }

    /**
     * @return float
     */
    public function getInitialPaymentRub(): float
    {
        return $this->initialPaymentRub;
    }

    /**
     * @param float $initialPaymentRub
     */
    public function setInitialPaymentRub(float $initialPaymentRub): void
    {
        $this->initialPaymentRub = $initialPaymentRub;
    }

    /**
     * @return string
     */
    public function getCreditProductId(): string
    {
        return $this->creditProductId;
    }

    /**
     * @param string $creditProductId
     */
    public function setCreditProductId(string $creditProductId): void
    {
        $this->creditProductId = $creditProductId;
    }

    /**
     * @return int
     */
    public function getAppType(): int
    {
        return $this->appType;
    }

    /**
     * @param int $appType
     */
    public function setAppType(int $appType): void
    {
        $this->appType = $appType;
    }

    /**
     * @return Borrower
     */
    public function getBorrower(): Borrower
    {
        return $this->borrower;
    }

    /**
     * @param Borrower $borrower
     */
    public function setBorrower(Borrower $borrower): void
    {
        $this->borrower = $borrower;
    }

    /**
     * @return ContactPerson
     */
    public function getCoBorrower1(): ContactPerson
    {
        return $this->coBorrower1;
    }

    /**
     * @param ContactPerson $coBorrower1
     */
    public function setCoBorrower1(ContactPerson $coBorrower1): void
    {
        $this->coBorrower1 = $coBorrower1;
    }

    /**
     * @return ContactPerson
     */
    public function getCoBorrower2(): ContactPerson
    {
        return $this->coBorrower2;
    }

    /**
     * @param ContactPerson $coBorrower2
     */
    public function setCoBorrower2(ContactPerson $coBorrower2): void
    {
        $this->coBorrower2 = $coBorrower2;
    }

    /**
     * @return ContactPerson
     */
    public function getContactPerson1(): ContactPerson
    {
        return $this->contactPerson1;
    }

    /**
     * @param ContactPerson $contactPerson1
     */
    public function setContactPerson1(ContactPerson $contactPerson1): void
    {
        $this->contactPerson1 = $contactPerson1;
    }

    /**
     * @return ContactPerson
     */
    public function getContactPerson2(): ContactPerson
    {
        return $this->contactPerson2;
    }

    /**
     * @param ContactPerson $contactPerson2
     */
    public function setContactPerson2(ContactPerson $contactPerson2): void
    {
        $this->contactPerson2 = $contactPerson2;
    }

    /**
     * @return Goods
     */
    public function getGoods(): Goods
    {
        return $this->goods;
    }

    /**
     * @param Goods $goods
     */
    public function setGoods(Goods $goods): void
    {
        $this->goods = $goods;
    }

    /**
     * @return Insurance
     */
    public function getInsurances(): Insurance
    {
        return $this->insurances;
    }

    /**
     * @param Insurance $insurances
     */
    public function setInsurances(Insurance $insurances): void
    {
        $this->insurances = $insurances;
    }

    /**
     * @return int
     */
    public function getServicePackageId(): int
    {
        return $this->servicePackageId;
    }

    /**
     * @param int $servicePackageId
     */
    public function setServicePackageId(int $servicePackageId): void
    {
        $this->servicePackageId = $servicePackageId;
    }

    /**
     * @return Scan
     */
    public function getBorrowerDocumentsScans(): Scan
    {
        return $this->borrowerDocumentsScans;
    }

    /**
     * @param Scan $borrowerDocumentsScans
     */
    public function setBorrowerDocumentsScans(Scan $borrowerDocumentsScans): void
    {
        $this->borrowerDocumentsScans = $borrowerDocumentsScans;
    }

    /**
     * @return bool
     */
    public function isTransferClaimDenied(): bool
    {
        return $this->transferClaimDenied;
    }

    /**
     * @param bool $transferClaimDenied
     */
    public function setTransferClaimDenied(bool $transferClaimDenied): void
    {
        $this->transferClaimDenied = $transferClaimDenied;
    }

    /**
     * @return int
     */
    public function getNeedCallCheck(): int
    {
        return $this->needCallCheck;
    }

    /**
     * @param int $needCallCheck
     */
    public function setNeedCallCheck(int $needCallCheck): void
    {
        $this->needCallCheck = $needCallCheck;
    }

    /**
     * @return bool
     */
    public function isAutoReject(): bool
    {
        return $this->autoReject;
    }

    /**
     * @param bool $autoReject
     */
    public function setAutoReject(bool $autoReject): void
    {
        $this->autoReject = $autoReject;
    }
}
