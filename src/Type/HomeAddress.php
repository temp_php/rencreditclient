<?php

namespace Rencredit\Type;

class HomeAddress
{

    /**
     * @var int
     */
    private $timeOfLivingInMonths;

    /**
     * @var int
     */
    private $propertyStatus;

    /**
     * @return int
     */
    public function getTimeOfLivingInMonths()
    {
        return $this->timeOfLivingInMonths;
    }

    /**
     * @param int $timeOfLivingInMonths
     * @return HomeAddress
     */
    public function withTimeOfLivingInMonths($timeOfLivingInMonths)
    {
        $new = clone $this;
        $new->timeOfLivingInMonths = $timeOfLivingInMonths;

        return $new;
    }

    /**
     * @return int
     */
    public function getPropertyStatus()
    {
        return $this->propertyStatus;
    }

    /**
     * @param int $propertyStatus
     * @return HomeAddress
     */
    public function withPropertyStatus($propertyStatus)
    {
        $new = clone $this;
        $new->propertyStatus = $propertyStatus;

        return $new;
    }


}

