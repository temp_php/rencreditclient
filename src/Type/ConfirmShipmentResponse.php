<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\ResultInterface;

class ConfirmShipmentResponse implements ResultInterface
{

    /**
     * @var int
     */
    private $code;

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return ConfirmShipmentResponse
     */
    public function withCode($code)
    {
        $new = clone $this;
        $new->code = $code;

        return $new;
    }


}

