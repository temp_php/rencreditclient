<?php

namespace Rencredit\Type;

class InfoInsurance
{

    /**
     * @var int
     */
    private $insuranceCode;

    /**
     * @return int
     */
    public function getInsuranceCode()
    {
        return $this->insuranceCode;
    }

    /**
     * @param int $insuranceCode
     * @return InfoInsurance
     */
    public function withInsuranceCode($insuranceCode)
    {
        $new = clone $this;
        $new->insuranceCode = $insuranceCode;

        return $new;
    }


}

