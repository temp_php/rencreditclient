<?php

namespace Rencredit\Type;

class CreditInfo
{

    /**
     * @var string
     */
    private $account;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var string
     */
    private $contract;

    /**
     * @var float
     */
    private $insurance;

    /**
     * @var float
     */
    private $irr;

    /**
     * @var string
     */
    private $oferta;

    /**
     * @var float
     */
    private $servicePackageSum;

    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param string $account
     * @return CreditInfo
     */
    public function withAccount($account)
    {
        $new = clone $this;
        $new->account = $account;

        return $new;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return CreditInfo
     */
    public function withAmount($amount)
    {
        $new = clone $this;
        $new->amount = $amount;

        return $new;
    }

    /**
     * @return string
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * @param string $contract
     * @return CreditInfo
     */
    public function withContract($contract)
    {
        $new = clone $this;
        $new->contract = $contract;

        return $new;
    }

    /**
     * @return float
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * @param float $insurance
     * @return CreditInfo
     */
    public function withInsurance($insurance)
    {
        $new = clone $this;
        $new->insurance = $insurance;

        return $new;
    }

    /**
     * @return float
     */
    public function getIrr()
    {
        return $this->irr;
    }

    /**
     * @param float $irr
     * @return CreditInfo
     */
    public function withIrr($irr)
    {
        $new = clone $this;
        $new->irr = $irr;

        return $new;
    }

    /**
     * @return string
     */
    public function getOferta()
    {
        return $this->oferta;
    }

    /**
     * @param string $oferta
     * @return CreditInfo
     */
    public function withOferta($oferta)
    {
        $new = clone $this;
        $new->oferta = $oferta;

        return $new;
    }

    /**
     * @return float
     */
    public function getServicePackageSum()
    {
        return $this->servicePackageSum;
    }

    /**
     * @param float $servicePackageSum
     * @return CreditInfo
     */
    public function withServicePackageSum($servicePackageSum)
    {
        $new = clone $this;
        $new->servicePackageSum = $servicePackageSum;

        return $new;
    }


}

