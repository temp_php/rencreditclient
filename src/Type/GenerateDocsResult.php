<?php

namespace Rencredit\Type;

class GenerateDocsResult
{

    /**
     * @var int
     */
    private $code;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \Rencredit\Type\CreditInfo
     */
    private $creditInfo;

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return GenerateDocsResult
     */
    public function withCode($code)
    {
        $new = clone $this;
        $new->code = $code;

        return $new;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return GenerateDocsResult
     */
    public function withComment($comment)
    {
        $new = clone $this;
        $new->comment = $comment;

        return $new;
    }

    /**
     * @return \Rencredit\Type\CreditInfo
     */
    public function getCreditInfo()
    {
        return $this->creditInfo;
    }

    /**
     * @param \Rencredit\Type\CreditInfo $creditInfo
     * @return GenerateDocsResult
     */
    public function withCreditInfo($creditInfo)
    {
        $new = clone $this;
        $new->creditInfo = $creditInfo;

        return $new;
    }


}

