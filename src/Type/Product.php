<?php

namespace Rencredit\Type;

class Product
{

    /**
     * @var string
     */
    private $productId;

    /**
     * @return string
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param string $productId
     * @return Product
     */
    public function withProductId($productId)
    {
        $new = clone $this;
        $new->productId = $productId;

        return $new;
    }


}

