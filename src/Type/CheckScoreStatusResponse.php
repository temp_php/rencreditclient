<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\ResultInterface;

class CheckScoreStatusResponse implements ResultInterface
{

    /**
     * @var \Rencredit\Type\Status
     */
    private $return;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var int
     */
    private $docSignType;

    /**
     * @return \Rencredit\Type\Status
     */
    public function getReturn()
    {
        return $this->return;
    }

    /**
     * @param \Rencredit\Type\Status $return
     * @return CheckScoreStatusResponse
     */
    public function withReturn($return)
    {
        $new = clone $this;
        $new->return = $return;

        return $new;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return CheckScoreStatusResponse
     */
    public function withComment($comment)
    {
        $new = clone $this;
        $new->comment = $comment;

        return $new;
    }

    /**
     * @return int
     */
    public function getDocSignType()
    {
        return $this->docSignType;
    }

    /**
     * @param int $docSignType
     * @return CheckScoreStatusResponse
     */
    public function withDocSignType($docSignType)
    {
        $new = clone $this;
        $new->docSignType = $docSignType;

        return $new;
    }


}

