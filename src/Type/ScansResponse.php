<?php

namespace Rencredit\Type;

class ScansResponse
{

    /**
     * @var int
     */
    private $docType;

    /**
     * @var int
     */
    private $response;

    /**
     * @return int
     */
    public function getDocType()
    {
        return $this->docType;
    }

    /**
     * @param int $docType
     * @return ScansResponse
     */
    public function withDocType($docType)
    {
        $new = clone $this;
        $new->docType = $docType;

        return $new;
    }

    /**
     * @return int
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param int $response
     * @return ScansResponse
     */
    public function withResponse($response)
    {
        $new = clone $this;
        $new->response = $response;

        return $new;
    }


}

