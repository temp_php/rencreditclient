<?php

namespace Rencredit\Type;

class Person
{

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var string
     */
    protected $patronymic;
}

