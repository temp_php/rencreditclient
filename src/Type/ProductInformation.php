<?php

namespace Rencredit\Type;

class ProductInformation
{

    /**
     * @var int
     */
    private $productGroup;

    /**
     * @var \Rencredit\Type\Product
     */
    private $products;

    /**
     * @var \Rencredit\Type\InfoServicePackage
     */
    private $servicePackages;

    /**
     * @var \Rencredit\Type\InfoInsurance
     */
    private $insurances;

    /**
     * @return int
     */
    public function getProductGroup()
    {
        return $this->productGroup;
    }

    /**
     * @param int $productGroup
     * @return ProductInformation
     */
    public function withProductGroup($productGroup)
    {
        $new = clone $this;
        $new->productGroup = $productGroup;

        return $new;
    }

    /**
     * @return \Rencredit\Type\Product
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param \Rencredit\Type\Product $products
     * @return ProductInformation
     */
    public function withProducts($products)
    {
        $new = clone $this;
        $new->products = $products;

        return $new;
    }

    /**
     * @return \Rencredit\Type\InfoServicePackage
     */
    public function getServicePackages()
    {
        return $this->servicePackages;
    }

    /**
     * @param \Rencredit\Type\InfoServicePackage $servicePackages
     * @return ProductInformation
     */
    public function withServicePackages($servicePackages)
    {
        $new = clone $this;
        $new->servicePackages = $servicePackages;

        return $new;
    }

    /**
     * @return \Rencredit\Type\InfoInsurance
     */
    public function getInsurances()
    {
        return $this->insurances;
    }

    /**
     * @param \Rencredit\Type\InfoInsurance $insurances
     * @return ProductInformation
     */
    public function withInsurances($insurances)
    {
        $new = clone $this;
        $new->insurances = $insurances;

        return $new;
    }


}

