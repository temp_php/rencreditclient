<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\RequestInterface;

class SendScansDocumentsRequest implements RequestInterface
{

    /**
     * @var string
     */
    private $correlationId;

    /**
     * @var int
     */
    private $authType;

    /**
     * @var \Rencredit\Type\DocScans
     */
    private $docScans;

    /**
     * Constructor
     *
     * @var string $correlationId
     * @var int $authType
     * @var \Rencredit\Type\DocScans $docScans
     */
    public function __construct($correlationId, $authType, $docScans)
    {
        $this->correlationId = $correlationId;
        $this->authType = $authType;
        $this->docScans = $docScans;
    }

    /**
     * @return string
     */
    public function getCorrelationId()
    {
        return $this->correlationId;
    }

    /**
     * @param string $correlationId
     * @return SendScansDocumentsRequest
     */
    public function withCorrelationId($correlationId)
    {
        $new = clone $this;
        $new->correlationId = $correlationId;

        return $new;
    }

    /**
     * @return int
     */
    public function getAuthType()
    {
        return $this->authType;
    }

    /**
     * @param int $authType
     * @return SendScansDocumentsRequest
     */
    public function withAuthType($authType)
    {
        $new = clone $this;
        $new->authType = $authType;

        return $new;
    }

    /**
     * @return \Rencredit\Type\DocScans
     */
    public function getDocScans()
    {
        return $this->docScans;
    }

    /**
     * @param \Rencredit\Type\DocScans $docScans
     * @return SendScansDocumentsRequest
     */
    public function withDocScans($docScans)
    {
        $new = clone $this;
        $new->docScans = $docScans;

        return $new;
    }


}

