<?php

namespace Rencredit\Type;

class ContactTypeFF
{

    /**
     * @var int
     */
    private $preferredContactType;

    /**
     * @var int
     */
    private $reportDeliveryAddress;

    /**
     * @var bool
     */
    private $reportDeliveryType;

    /**
     * @return int
     */
    public function getPreferredContactType()
    {
        return $this->preferredContactType;
    }

    /**
     * @param int $preferredContactType
     * @return ContactTypeFF
     */
    public function withPreferredContactType($preferredContactType)
    {
        $new = clone $this;
        $new->preferredContactType = $preferredContactType;

        return $new;
    }

    /**
     * @return int
     */
    public function getReportDeliveryAddress()
    {
        return $this->reportDeliveryAddress;
    }

    /**
     * @param int $reportDeliveryAddress
     * @return ContactTypeFF
     */
    public function withReportDeliveryAddress($reportDeliveryAddress)
    {
        $new = clone $this;
        $new->reportDeliveryAddress = $reportDeliveryAddress;

        return $new;
    }

    /**
     * @return bool
     */
    public function getReportDeliveryType()
    {
        return $this->reportDeliveryType;
    }

    /**
     * @param bool $reportDeliveryType
     * @return ContactTypeFF
     */
    public function withReportDeliveryType($reportDeliveryType)
    {
        $new = clone $this;
        $new->reportDeliveryType = $reportDeliveryType;

        return $new;
    }


}

