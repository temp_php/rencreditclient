<?php

namespace Rencredit\Type;

class ScansStatus
{

    /**
     * @var int
     */
    private $docType;

    /**
     * @var int
     */
    private $response;

    /**
     * @var string
     */
    private $reason;

    /**
     * @return int
     */
    public function getDocType()
    {
        return $this->docType;
    }

    /**
     * @param int $docType
     * @return ScansStatus
     */
    public function withDocType($docType)
    {
        $new = clone $this;
        $new->docType = $docType;

        return $new;
    }

    /**
     * @return int
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param int $response
     * @return ScansStatus
     */
    public function withResponse($response)
    {
        $new = clone $this;
        $new->response = $response;

        return $new;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     * @return ScansStatus
     */
    public function withReason($reason)
    {
        $new = clone $this;
        $new->reason = $reason;

        return $new;
    }


}

