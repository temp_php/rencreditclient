<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\ResultInterface;

class TransferForResigningResponse implements ResultInterface
{

    /**
     * @var int
     */
    private $resultCode;

    /**
     * @return int
     */
    public function getResultCode()
    {
        return $this->resultCode;
    }

    /**
     * @param int $resultCode
     * @return TransferForResigningResponse
     */
    public function withResultCode($resultCode)
    {
        $new = clone $this;
        $new->resultCode = $resultCode;

        return $new;
    }


}

