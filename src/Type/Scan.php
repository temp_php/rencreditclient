<?php

namespace Rencredit\Type;

class Scan
{

    /**
     * @var int
     */
    private $docType;

    /**
     * @var string
     */
    private $docScan;

    /**
     * Scan constructor.
     * @param int $docType
     * @param string $docScan
     */
    public function __construct(int $docType, string $docScan)
    {
        $this->docType = $docType;
        $this->docScan = $docScan;
    }

    /**
     * @return int
     */
    public function getDocType()
    {
        return $this->docType;
    }

    /**
     * @param int $docType
     * @return Scan
     */
    public function withDocType($docType)
    {
        $new = clone $this;
        $new->docType = $docType;

        return $new;
    }

    /**
     * @return string
     */
    public function getDocScan()
    {
        return $this->docScan;
    }

    /**
     * @param string $docScan
     * @return Scan
     */
    public function withDocScan($docScan)
    {
        $new = clone $this;
        $new->docScan = $docScan;

        return $new;
    }


}

