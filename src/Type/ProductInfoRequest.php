<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\RequestInterface;

class ProductInfoRequest implements RequestInterface
{

    /**
     * @var int
     */
    private $posId;

    /**
     * Constructor
     *
     * @var int $posId
     */
    public function __construct($posId)
    {
        $this->posId = $posId;
    }

    /**
     * @return int
     */
    public function getPosId()
    {
        return $this->posId;
    }

    /**
     * @param int $posId
     * @return ProductInfoRequest
     */
    public function withPosId($posId)
    {
        $new = clone $this;
        $new->posId = $posId;

        return $new;
    }


}

