<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\RequestInterface;

class AdditionalScoreRequest implements RequestInterface
{

    /**
     * @var string
     */
    private $correlationId;

    /**
     * @var \Rencredit\Type\Form
     */
    private $application;

    /**
     * Constructor
     *
     * @var string $correlationId
     * @var \Rencredit\Type\Form $application
     */
    public function __construct($correlationId, $application)
    {
        $this->correlationId = $correlationId;
        $this->application = $application;
    }

    /**
     * @return string
     */
    public function getCorrelationId()
    {
        return $this->correlationId;
    }

    /**
     * @param string $correlationId
     * @return AdditionalScoreRequest
     */
    public function withCorrelationId($correlationId)
    {
        $new = clone $this;
        $new->correlationId = $correlationId;

        return $new;
    }

    /**
     * @return \Rencredit\Type\Form
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @param \Rencredit\Type\Form $application
     * @return AdditionalScoreRequest
     */
    public function withApplication($application)
    {
        $new = clone $this;
        $new->application = $application;

        return $new;
    }


}

