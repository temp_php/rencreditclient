<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\RequestInterface;

class ScoreRequest implements RequestInterface
{

    /**
     * @var \Rencredit\Type\Form
     */
    private $application;

    /**
     * Constructor
     *
     * @var \Rencredit\Type\Form $application
     */
    public function __construct($application)
    {
        $this->application = $application;
    }

    /**
     * @return \Rencredit\Type\Form
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @param \Rencredit\Type\Form $application
     * @return ScoreRequest
     */
    public function withApplication($application)
    {
        $new = clone $this;
        $new->application = $application;

        return $new;
    }


}

