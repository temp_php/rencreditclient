<?php

namespace Rencredit\Type;

class IForm extends BasicForm
{

    /**
     * @var \Rencredit\Type\IBorrower
     */
    private $borrower;

    /**
     * @var \Rencredit\Type\Goods
     */
    private $goods;

    /**
     * @var \Rencredit\Type\Insurance
     */
    private $insurances;

    /**
     * @var int
     */
    private $servicePackageId;

    /**
     * @var bool
     */
    private $transferClaimDenied;

    /**
     * @var int
     */
    private $needCallCheck;

    /**
     * @var bool
     */
    private $autoReject;

    /**
     * @return \Rencredit\Type\IBorrower
     */
    public function getBorrower()
    {
        return $this->borrower;
    }

    /**
     * @param \Rencredit\Type\IBorrower $borrower
     * @return IForm
     */
    public function withBorrower($borrower)
    {
        $new = clone $this;
        $new->borrower = $borrower;

        return $new;
    }

    /**
     * @return \Rencredit\Type\Goods
     */
    public function getGoods()
    {
        return $this->goods;
    }

    /**
     * @param \Rencredit\Type\Goods $goods
     * @return IForm
     */
    public function withGoods($goods)
    {
        $new = clone $this;
        $new->goods = $goods;

        return $new;
    }

    /**
     * @return \Rencredit\Type\Insurance
     */
    public function getInsurances()
    {
        return $this->insurances;
    }

    /**
     * @param \Rencredit\Type\Insurance $insurances
     * @return IForm
     */
    public function withInsurances($insurances)
    {
        $new = clone $this;
        $new->insurances = $insurances;

        return $new;
    }

    /**
     * @return int
     */
    public function getServicePackageId()
    {
        return $this->servicePackageId;
    }

    /**
     * @param int $servicePackageId
     * @return IForm
     */
    public function withServicePackageId($servicePackageId)
    {
        $new = clone $this;
        $new->servicePackageId = $servicePackageId;

        return $new;
    }

    /**
     * @return bool
     */
    public function getTransferClaimDenied()
    {
        return $this->transferClaimDenied;
    }

    /**
     * @param bool $transferClaimDenied
     * @return IForm
     */
    public function withTransferClaimDenied($transferClaimDenied)
    {
        $new = clone $this;
        $new->transferClaimDenied = $transferClaimDenied;

        return $new;
    }

    /**
     * @return int
     */
    public function getNeedCallCheck()
    {
        return $this->needCallCheck;
    }

    /**
     * @param int $needCallCheck
     * @return IForm
     */
    public function withNeedCallCheck($needCallCheck)
    {
        $new = clone $this;
        $new->needCallCheck = $needCallCheck;

        return $new;
    }

    /**
     * @return bool
     */
    public function getAutoReject()
    {
        return $this->autoReject;
    }

    /**
     * @param bool $autoReject
     * @return IForm
     */
    public function withAutoReject($autoReject)
    {
        $new = clone $this;
        $new->autoReject = $autoReject;

        return $new;
    }


}

