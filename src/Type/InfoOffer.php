<?php

namespace Rencredit\Type;

class InfoOffer
{

    /**
     * @var float
     */
    private $creditLimit;

    /**
     * @var \Rencredit\Type\CreditPeriod
     */
    private $creditPeriods;

    /**
     * @return float
     */
    public function getCreditLimit()
    {
        return $this->creditLimit;
    }

    /**
     * @param float $creditLimit
     * @return InfoOffer
     */
    public function withCreditLimit($creditLimit)
    {
        $new = clone $this;
        $new->creditLimit = $creditLimit;

        return $new;
    }

    /**
     * @return \Rencredit\Type\CreditPeriod
     */
    public function getCreditPeriods()
    {
        return $this->creditPeriods;
    }

    /**
     * @param \Rencredit\Type\CreditPeriod $creditPeriods
     * @return InfoOffer
     */
    public function withCreditPeriods($creditPeriods)
    {
        $new = clone $this;
        $new->creditPeriods = $creditPeriods;

        return $new;
    }


}

