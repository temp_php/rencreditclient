<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\RequestInterface;

class ProofIncomeRequest implements RequestInterface
{

    /**
     * @var string
     */
    private $correlationId;

    /**
     * @var \Rencredit\Type\IncomeDocScan
     */
    private $docScans;

    /**
     * Constructor
     *
     * @var string $correlationId
     * @var \Rencredit\Type\IncomeDocScan $docScans
     */
    public function __construct($correlationId, $docScans)
    {
        $this->correlationId = $correlationId;
        $this->docScans = $docScans;
    }

    /**
     * @return string
     */
    public function getCorrelationId()
    {
        return $this->correlationId;
    }

    /**
     * @param string $correlationId
     * @return ProofIncomeRequest
     */
    public function withCorrelationId($correlationId)
    {
        $new = clone $this;
        $new->correlationId = $correlationId;

        return $new;
    }

    /**
     * @return \Rencredit\Type\IncomeDocScan
     */
    public function getDocScans()
    {
        return $this->docScans;
    }

    /**
     * @param \Rencredit\Type\IncomeDocScan $docScans
     * @return ProofIncomeRequest
     */
    public function withDocScans($docScans)
    {
        $new = clone $this;
        $new->docScans = $docScans;

        return $new;
    }


}

