<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\ResultInterface;

class ProofIncomeResponse implements ResultInterface
{

    /**
     * @var \Rencredit\Type\ScansResponse
     */
    private $return;

    /**
     * @var int
     */
    private $result;

    /**
     * @return \Rencredit\Type\ScansResponse
     */
    public function getReturn()
    {
        return $this->return;
    }

    /**
     * @param \Rencredit\Type\ScansResponse $return
     * @return ProofIncomeResponse
     */
    public function withReturn($return)
    {
        $new = clone $this;
        $new->return = $return;

        return $new;
    }

    /**
     * @return int
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param int $result
     * @return ProofIncomeResponse
     */
    public function withResult($result)
    {
        $new = clone $this;
        $new->result = $result;

        return $new;
    }


}

