<?php

namespace Rencredit\Type;

class Status
{

    /**
     * @var int
     */
    private $code;

    /**
     * @var \Rencredit\Type\Insurance
     */
    private $insurance;

    /**
     * @var \Rencredit\Type\Offer
     */
    private $offer;

    /**
     * @var \Rencredit\Type\InfoOffer
     */
    private $infoOffer;

    /**
     * @var float
     */
    private $goodsSumPrice;

    /**
     * @var \Rencredit\Type\ServicePackages
     */
    private $servicePackages;

    /**
     * @var \Rencredit\Type\ScanInfo
     */
    private $scans;

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return Status
     */
    public function withCode($code)
    {
        $new = clone $this;
        $new->code = $code;

        return $new;
    }

    /**
     * @return \Rencredit\Type\Insurance
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * @param \Rencredit\Type\Insurance $insurance
     * @return Status
     */
    public function withInsurance($insurance)
    {
        $new = clone $this;
        $new->insurance = $insurance;

        return $new;
    }

    /**
     * @return \Rencredit\Type\Offer
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param \Rencredit\Type\Offer $offer
     * @return Status
     */
    public function withOffer($offer)
    {
        $new = clone $this;
        $new->offer = $offer;

        return $new;
    }

    /**
     * @return \Rencredit\Type\InfoOffer
     */
    public function getInfoOffer()
    {
        return $this->infoOffer;
    }

    /**
     * @param \Rencredit\Type\InfoOffer $infoOffer
     * @return Status
     */
    public function withInfoOffer($infoOffer)
    {
        $new = clone $this;
        $new->infoOffer = $infoOffer;

        return $new;
    }

    /**
     * @return float
     */
    public function getGoodsSumPrice()
    {
        return $this->goodsSumPrice;
    }

    /**
     * @param float $goodsSumPrice
     * @return Status
     */
    public function withGoodsSumPrice($goodsSumPrice)
    {
        $new = clone $this;
        $new->goodsSumPrice = $goodsSumPrice;

        return $new;
    }

    /**
     * @return \Rencredit\Type\ServicePackages
     */
    public function getServicePackages()
    {
        return $this->servicePackages;
    }

    /**
     * @param \Rencredit\Type\ServicePackages $servicePackages
     * @return Status
     */
    public function withServicePackages($servicePackages)
    {
        $new = clone $this;
        $new->servicePackages = $servicePackages;

        return $new;
    }

    /**
     * @return \Rencredit\Type\ScanInfo
     */
    public function getScans()
    {
        return $this->scans;
    }

    /**
     * @param \Rencredit\Type\ScanInfo $scans
     * @return Status
     */
    public function withScans($scans)
    {
        $new = clone $this;
        $new->scans = $scans;

        return $new;
    }


}

