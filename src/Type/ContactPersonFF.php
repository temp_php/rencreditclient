<?php

namespace Rencredit\Type;

class ContactPersonFF
{

    /**
     * @var int
     */
    private $relation;

    /**
     * @var \Rencredit\Type\MobilePhone
     */
    private $mobilePhone;

    /**
     * @var \Rencredit\Type\StationaryPhone
     */
    private $homePhone;

    /**
     * @return int
     */
    public function getRelation()
    {
        return $this->relation;
    }

    /**
     * @param int $relation
     * @return ContactPersonFF
     */
    public function withRelation($relation)
    {
        $new = clone $this;
        $new->relation = $relation;

        return $new;
    }

    /**
     * @return \Rencredit\Type\MobilePhone
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * @param \Rencredit\Type\MobilePhone $mobilePhone
     * @return ContactPersonFF
     */
    public function withMobilePhone($mobilePhone)
    {
        $new = clone $this;
        $new->mobilePhone = $mobilePhone;

        return $new;
    }

    /**
     * @return \Rencredit\Type\StationaryPhone
     */
    public function getHomePhone()
    {
        return $this->homePhone;
    }

    /**
     * @param \Rencredit\Type\StationaryPhone $homePhone
     * @return ContactPersonFF
     */
    public function withHomePhone($homePhone)
    {
        $new = clone $this;
        $new->homePhone = $homePhone;

        return $new;
    }


}

