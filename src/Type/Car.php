<?php

namespace Rencredit\Type;

class Car
{

    /**
     * @var string
     */
    private $assemblyYear;

    /**
     * @var string
     */
    private $model;

    /**
     * @var string
     */
    private $number;

    /**
     * Car constructor.
     * @param string $assemblyYear
     * @param string $model
     * @param string $number
     */
    public function __construct(string $assemblyYear, string $model, string $number)
    {
        $this->assemblyYear = $assemblyYear;
        $this->model = $model;
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getAssemblyYear()
    {
        return $this->assemblyYear;
    }

    /**
     * @param string $assemblyYear
     * @return Car
     */
    public function withAssemblyYear($assemblyYear)
    {
        $new = clone $this;
        $new->assemblyYear = $assemblyYear;

        return $new;
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param string $model
     * @return Car
     */
    public function withModel($model)
    {
        $new = clone $this;
        $new->model = $model;

        return $new;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Car
     */
    public function withNumber($number)
    {
        $new = clone $this;
        $new->number = $number;

        return $new;
    }


}

