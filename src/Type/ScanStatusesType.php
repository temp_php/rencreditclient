<?php

namespace Rencredit\Type;

class ScanStatusesType
{

    /**
     * @var \Rencredit\Type\ScansStatus
     */
    private $scansStatus;

    /**
     * @var int
     */
    private $code;

    /**
     * @var string
     */
    private $comment;

    /**
     * @return \Rencredit\Type\ScansStatus
     */
    public function getScansStatus()
    {
        return $this->scansStatus;
    }

    /**
     * @param \Rencredit\Type\ScansStatus $scansStatus
     * @return ScanStatusesType
     */
    public function withScansStatus($scansStatus)
    {
        $new = clone $this;
        $new->scansStatus = $scansStatus;

        return $new;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return ScanStatusesType
     */
    public function withCode($code)
    {
        $new = clone $this;
        $new->code = $code;

        return $new;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return ScanStatusesType
     */
    public function withComment($comment)
    {
        $new = clone $this;
        $new->comment = $comment;

        return $new;
    }


}

