<?php

namespace Rencredit\Type;

class DownPayment
{

    /**
     * @var float
     */
    private $downPayment;

    /**
     * @return float
     */
    public function getDownPayment()
    {
        return $this->downPayment;
    }

    /**
     * @param float $downPayment
     * @return DownPayment
     */
    public function withDownPayment($downPayment)
    {
        $new = clone $this;
        $new->downPayment = $downPayment;

        return $new;
    }


}

