<?php

namespace Rencredit\Type;

class Employment
{

    /**
     * @var int
     */
    private $activitiesType;

    /**
     * @var string
     */
    private $employeeFrom;

    /**
     * @var \Rencredit\Type\Address
     */
    private $employerAddress;

    /**
     * @var string
     */
    private $employerName;

    /**
     * @var \Rencredit\Type\Phone
     */
    private $employerPhone;

    /**
     * @var string
     */
    private $employerWebSite;

    /**
     * @var string
     */
    private $occupation;

    /**
     * @var int
     */
    private $occupationType;

    /**
     * @var \DateTimeInterface
     */
    private $fromDate;

    /**
     * @var \Rencredit\Type\StationaryPhone
     */
    private $phone;

    /**
     * @var \Rencredit\Type\StationaryPhone
     */
    private $fax;

    /**
     * @var string
     */
    private $webSite;

    /**
     * @var int
     */
    private $type;

    /**
     * @var bool
     */
    private $employmentType;

    /**
     * @var float
     */
    private $monthlyIncomeInRub;

    /**
     * @param int $activitiesType
     */
    public function setActivitiesType(int $activitiesType): void
    {
        $this->activitiesType = $activitiesType;
    }

    /**
     * @param string $employeeFrom
     */
    public function setEmployeeFrom(string $employeeFrom): void
    {
        $this->employeeFrom = $employeeFrom;
    }

    /**
     * @param Address $employerAddress
     */
    public function setEmployerAddress(Address $employerAddress): void
    {
        $this->employerAddress = $employerAddress;
    }

    /**
     * @param string $employerName
     */
    public function setEmployerName(string $employerName): void
    {
        $this->employerName = $employerName;
    }

    /**
     * @param Phone $employerPhone
     */
    public function setEmployerPhone(Phone $employerPhone): void
    {
        $this->employerPhone = $employerPhone;
    }

    /**
     * @param string $employerWebSite
     */
    public function setEmployerWebSite(string $employerWebSite): void
    {
        $this->employerWebSite = $employerWebSite;
    }

    /**
     * @param string $occupation
     */
    public function setOccupation(string $occupation): void
    {
        $this->occupation = $occupation;
    }

    /**
     * @param int $occupationType
     */
    public function setOccupationType(int $occupationType): void
    {
        $this->occupationType = $occupationType;
    }

    /**
     * @param \DateTimeInterface $fromDate
     */
    public function setFromDate(\DateTimeInterface $fromDate): void
    {
        $this->fromDate = $fromDate;
    }

    /**
     * @param StationaryPhone $phone
     */
    public function setPhone(StationaryPhone $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @param StationaryPhone $fax
     */
    public function setFax(StationaryPhone $fax): void
    {
        $this->fax = $fax;
    }

    /**
     * @param string $webSite
     */
    public function setWebSite(string $webSite): void
    {
        $this->webSite = $webSite;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @param bool $employmentType
     */
    public function setEmploymentType(bool $employmentType): void
    {
        $this->employmentType = $employmentType;
    }

    /**
     * @param float $monthlyIncomeInRub
     */
    public function setMonthlyIncomeInRub(float $monthlyIncomeInRub): void
    {
        $this->monthlyIncomeInRub = $monthlyIncomeInRub;
    }

    /**
     * @return int
     */
    public function getActivitiesType()
    {
        return $this->activitiesType;
    }

    /**
     * @param int $activitiesType
     * @return Employment
     */
    public function withActivitiesType($activitiesType)
    {
        $new = clone $this;
        $new->activitiesType = $activitiesType;

        return $new;
    }

    /**
     * @return string
     */
    public function getEmployeeFrom()
    {
        return $this->employeeFrom;
    }

    /**
     * @param string $employeeFrom
     * @return Employment
     */
    public function withEmployeeFrom($employeeFrom)
    {
        $new = clone $this;
        $new->employeeFrom = $employeeFrom;

        return $new;
    }

    /**
     * @return \Rencredit\Type\Address
     */
    public function getEmployerAddress()
    {
        return $this->employerAddress;
    }

    /**
     * @param \Rencredit\Type\Address $employerAddress
     * @return Employment
     */
    public function withEmployerAddress($employerAddress)
    {
        $new = clone $this;
        $new->employerAddress = $employerAddress;

        return $new;
    }

    /**
     * @return string
     */
    public function getEmployerName()
    {
        return $this->employerName;
    }

    /**
     * @param string $employerName
     * @return Employment
     */
    public function withEmployerName($employerName)
    {
        $new = clone $this;
        $new->employerName = $employerName;

        return $new;
    }

    /**
     * @return \Rencredit\Type\Phone
     */
    public function getEmployerPhone()
    {
        return $this->employerPhone;
    }

    /**
     * @param \Rencredit\Type\Phone $employerPhone
     * @return Employment
     */
    public function withEmployerPhone($employerPhone)
    {
        $new = clone $this;
        $new->employerPhone = $employerPhone;

        return $new;
    }

    /**
     * @return string
     */
    public function getEmployerWebSite()
    {
        return $this->employerWebSite;
    }

    /**
     * @param string $employerWebSite
     * @return Employment
     */
    public function withEmployerWebSite($employerWebSite)
    {
        $new = clone $this;
        $new->employerWebSite = $employerWebSite;

        return $new;
    }

    /**
     * @return string
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * @param string $occupation
     * @return Employment
     */
    public function withOccupation($occupation)
    {
        $new = clone $this;
        $new->occupation = $occupation;

        return $new;
    }

    /**
     * @return int
     */
    public function getOccupationType()
    {
        return $this->occupationType;
    }

    /**
     * @param int $occupationType
     * @return Employment
     */
    public function withOccupationType($occupationType)
    {
        $new = clone $this;
        $new->occupationType = $occupationType;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * @param \DateTimeInterface $fromDate
     * @return Employment
     */
    public function withFromDate($fromDate)
    {
        $new = clone $this;
        $new->fromDate = $fromDate;

        return $new;
    }

    /**
     * @return \Rencredit\Type\StationaryPhone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param \Rencredit\Type\StationaryPhone $phone
     * @return Employment
     */
    public function withPhone($phone)
    {
        $new = clone $this;
        $new->phone = $phone;

        return $new;
    }

    /**
     * @return \Rencredit\Type\StationaryPhone
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param \Rencredit\Type\StationaryPhone $fax
     * @return Employment
     */
    public function withFax($fax)
    {
        $new = clone $this;
        $new->fax = $fax;

        return $new;
    }

    /**
     * @return string
     */
    public function getWebSite()
    {
        return $this->webSite;
    }

    /**
     * @param string $webSite
     * @return Employment
     */
    public function withWebSite($webSite)
    {
        $new = clone $this;
        $new->webSite = $webSite;

        return $new;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return Employment
     */
    public function withType($type)
    {
        $new = clone $this;
        $new->type = $type;

        return $new;
    }

    /**
     * @return bool
     */
    public function getEmploymentType()
    {
        return $this->employmentType;
    }

    /**
     * @param bool $employmentType
     * @return Employment
     */
    public function withEmploymentType($employmentType)
    {
        $new = clone $this;
        $new->employmentType = $employmentType;

        return $new;
    }

    /**
     * @return float
     */
    public function getMonthlyIncomeInRub()
    {
        return $this->monthlyIncomeInRub;
    }

    /**
     * @param float $monthlyIncomeInRub
     * @return Employment
     */
    public function withMonthlyIncomeInRub($monthlyIncomeInRub)
    {
        $new = clone $this;
        $new->monthlyIncomeInRub = $monthlyIncomeInRub;

        return $new;
    }


}

