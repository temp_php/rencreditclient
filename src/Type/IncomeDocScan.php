<?php

namespace Rencredit\Type;

class IncomeDocScan
{

    /**
     * @var int
     */
    private $docType;

    /**
     * @var string
     */
    private $docScan;

    /**
     * @var string
     */
    private $login;

    /**
     * @return int
     */
    public function getDocType()
    {
        return $this->docType;
    }

    /**
     * @param int $docType
     * @return IncomeDocScan
     */
    public function withDocType($docType)
    {
        $new = clone $this;
        $new->docType = $docType;

        return $new;
    }

    /**
     * @return string
     */
    public function getDocScan()
    {
        return $this->docScan;
    }

    /**
     * @param string $docScan
     * @return IncomeDocScan
     */
    public function withDocScan($docScan)
    {
        $new = clone $this;
        $new->docScan = $docScan;

        return $new;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     * @return IncomeDocScan
     */
    public function withLogin($login)
    {
        $new = clone $this;
        $new->login = $login;

        return $new;
    }


}

