<?php

namespace Rencredit\Type;

class InfoServicePackage
{

    /**
     * @var int
     */
    private $packageId;

    /**
     * @var string
     */
    private $packageName;

    /**
     * @return int
     */
    public function getPackageId()
    {
        return $this->packageId;
    }

    /**
     * @param int $packageId
     * @return InfoServicePackage
     */
    public function withPackageId($packageId)
    {
        $new = clone $this;
        $new->packageId = $packageId;

        return $new;
    }

    /**
     * @return string
     */
    public function getPackageName()
    {
        return $this->packageName;
    }

    /**
     * @param string $packageName
     * @return InfoServicePackage
     */
    public function withPackageName($packageName)
    {
        $new = clone $this;
        $new->packageName = $packageName;

        return $new;
    }


}

