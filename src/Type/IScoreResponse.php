<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\ResultInterface;

class IScoreResponse implements ResultInterface
{

    /**
     * @var string
     */
    private $correlationId;

    /**
     * @var int
     */
    private $code;

    /**
     * @return string
     */
    public function getCorrelationId()
    {
        return $this->correlationId;
    }

    /**
     * @param string $correlationId
     * @return IScoreResponse
     */
    public function withCorrelationId($correlationId)
    {
        $new = clone $this;
        $new->correlationId = $correlationId;

        return $new;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return IScoreResponse
     */
    public function withCode($code)
    {
        $new = clone $this;
        $new->code = $code;

        return $new;
    }


}

