<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\RequestInterface;

class CancelRequest implements RequestInterface
{

    /**
     * @var string
     */
    private $applicationId;

    /**
     * @var string
     */
    private $comment;

    /**
     * Constructor
     *
     * @var string $applicationId
     * @var string $comment
     */
    public function __construct($applicationId, $comment)
    {
        $this->applicationId = $applicationId;
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getApplicationId()
    {
        return $this->applicationId;
    }

    /**
     * @param string $applicationId
     * @return CancelRequest
     */
    public function withApplicationId($applicationId)
    {
        $new = clone $this;
        $new->applicationId = $applicationId;

        return $new;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return CancelRequest
     */
    public function withComment($comment)
    {
        $new = clone $this;
        $new->comment = $comment;

        return $new;
    }


}

