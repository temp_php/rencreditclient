<?php

namespace Rencredit\Type;

use Phpro\SoapClient\Type\RequestInterface;

class IScoreRequest implements RequestInterface
{

    /**
     * @var \Rencredit\Type\IForm
     */
    private $iApplication;

    /**
     * Constructor
     *
     * @var \Rencredit\Type\IForm $iApplication
     */
    public function __construct($iApplication)
    {
        $this->iApplication = $iApplication;
    }

    /**
     * @return \Rencredit\Type\IForm
     */
    public function getIApplication()
    {
        return $this->iApplication;
    }

    /**
     * @param \Rencredit\Type\IForm $iApplication
     * @return IScoreRequest
     */
    public function withIApplication($iApplication)
    {
        $new = clone $this;
        $new->iApplication = $iApplication;

        return $new;
    }


}

