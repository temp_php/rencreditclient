<?php

namespace Rencredit\Type;

class RealEstate
{

    /**
     * @var \Rencredit\Type\Address
     */
    private $address;

    /**
     * @var int
     */
    private $rooms;

    /**
     * @var float
     */
    private $floorspace;

    /**
     * @var float
     */
    private $share;

    /**
     * @var \Rencredit\Type\BelongingsParameters
     */
    private $parameters;

    /**
     * @return \Rencredit\Type\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param \Rencredit\Type\Address $address
     * @return RealEstate
     */
    public function withAddress($address)
    {
        $new = clone $this;
        $new->address = $address;

        return $new;
    }

    /**
     * @return int
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param int $rooms
     * @return RealEstate
     */
    public function withRooms($rooms)
    {
        $new = clone $this;
        $new->rooms = $rooms;

        return $new;
    }

    /**
     * @return float
     */
    public function getFloorspace()
    {
        return $this->floorspace;
    }

    /**
     * @param float $floorspace
     * @return RealEstate
     */
    public function withFloorspace($floorspace)
    {
        $new = clone $this;
        $new->floorspace = $floorspace;

        return $new;
    }

    /**
     * @return float
     */
    public function getShare()
    {
        return $this->share;
    }

    /**
     * @param float $share
     * @return RealEstate
     */
    public function withShare($share)
    {
        $new = clone $this;
        $new->share = $share;

        return $new;
    }

    /**
     * @return \Rencredit\Type\BelongingsParameters
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param \Rencredit\Type\BelongingsParameters $parameters
     * @return RealEstate
     */
    public function withParameters($parameters)
    {
        $new = clone $this;
        $new->parameters = $parameters;

        return $new;
    }


}

