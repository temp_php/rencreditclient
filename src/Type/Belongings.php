<?php

namespace Rencredit\Type;

class Belongings
{

    /**
     * @var \Rencredit\Type\Chattels
     */
    private $chattels;

    /**
     * @var \Rencredit\Type\RealEstate
     */
    private $realEstate;

    /**
     * @var \Rencredit\Type\BelongingsParameters
     */
    private $otherBelongings;

    /**
     * @return \Rencredit\Type\Chattels
     */
    public function getChattels()
    {
        return $this->chattels;
    }

    /**
     * @param \Rencredit\Type\Chattels $chattels
     * @return Belongings
     */
    public function withChattels($chattels)
    {
        $new = clone $this;
        $new->chattels = $chattels;

        return $new;
    }

    /**
     * @return \Rencredit\Type\RealEstate
     */
    public function getRealEstate()
    {
        return $this->realEstate;
    }

    /**
     * @param \Rencredit\Type\RealEstate $realEstate
     * @return Belongings
     */
    public function withRealEstate($realEstate)
    {
        $new = clone $this;
        $new->realEstate = $realEstate;

        return $new;
    }

    /**
     * @return \Rencredit\Type\BelongingsParameters
     */
    public function getOtherBelongings()
    {
        return $this->otherBelongings;
    }

    /**
     * @param \Rencredit\Type\BelongingsParameters $otherBelongings
     * @return Belongings
     */
    public function withOtherBelongings($otherBelongings)
    {
        $new = clone $this;
        $new->otherBelongings = $otherBelongings;

        return $new;
    }


}

