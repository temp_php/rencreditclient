<?php

namespace Rencredit\Type;

class IdDocument
{

    /**
     * @var mixed
     */
    private $documentType;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $series;

    /**
     * @var string
     */
    private $issueOrg;

    /**
     * @var \DateTimeInterface
     */
    private $issueDate;

    /**
     * @return mixed
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * @param mixed $documentType
     * @return IdDocument
     */
    public function withDocumentType($documentType)
    {
        $new = clone $this;
        $new->documentType = $documentType;

        return $new;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return IdDocument
     */
    public function withNumber($number)
    {
        $new = clone $this;
        $new->number = $number;

        return $new;
    }

    /**
     * @return string
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * @param string $series
     * @return IdDocument
     */
    public function withSeries($series)
    {
        $new = clone $this;
        $new->series = $series;

        return $new;
    }

    /**
     * @return string
     */
    public function getIssueOrg()
    {
        return $this->issueOrg;
    }

    /**
     * @param string $issueOrg
     * @return IdDocument
     */
    public function withIssueOrg($issueOrg)
    {
        $new = clone $this;
        $new->issueOrg = $issueOrg;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }

    /**
     * @param \DateTimeInterface $issueDate
     * @return IdDocument
     */
    public function withIssueDate($issueDate)
    {
        $new = clone $this;
        $new->issueDate = $issueDate;

        return $new;
    }


}

