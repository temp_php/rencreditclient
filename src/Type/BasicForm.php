<?php

namespace Rencredit\Type;

class BasicForm
{

    /**
     * @var int
     */
    protected $posId;

    /**
     * @var string
     */
    protected $webShopLink;

    /**
     * @var string
     */
    protected $userLogin;

    /**
     * @var string
     */
    protected $partnerID;

    /**
     * @var string
     */
    protected $partnerNetId;

    /**
     * @var int
     */
    protected $creditPeriodInMonths;

    /**
     * @var float
     */
    protected $initialPayment;

    /**
     * @var float
     */
    protected $initialPaymentRub;

    /**
     * @var string
     */
    protected $creditProductId;

    /**
     * @var int
     */
    protected $appType;
}

