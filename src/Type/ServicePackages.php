<?php

namespace Rencredit\Type;

class ServicePackages
{

    /**
     * @var int
     */
    private $packageId;

    /**
     * @var float
     */
    private $packagePrice;

    /**
     * @return int
     */
    public function getPackageId()
    {
        return $this->packageId;
    }

    /**
     * @param int $packageId
     * @return ServicePackages
     */
    public function withPackageId($packageId)
    {
        $new = clone $this;
        $new->packageId = $packageId;

        return $new;
    }

    /**
     * @return float
     */
    public function getPackagePrice()
    {
        return $this->packagePrice;
    }

    /**
     * @param float $packagePrice
     * @return ServicePackages
     */
    public function withPackagePrice($packagePrice)
    {
        $new = clone $this;
        $new->packagePrice = $packagePrice;

        return $new;
    }


}

