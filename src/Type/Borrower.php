<?php

namespace Rencredit\Type;

class Borrower extends Person
{

    /**
     * @var \DateTimeInterface
     */
    private $birthDate;

    /**
     * @var string
     */
    private $birthPlace;

    /**
     * @var \Rencredit\Type\Address
     */
    private $contactAddress;

    /**
     * @var string
     */
    private $creditHistorySubjectCode;

    /**
     * @var \Rencredit\Type\Passport
     */
    private $document1;

    /**
     * @var \Rencredit\Type\Document
     */
    private $document2;

    /**
     * @var string
     */
    private $snils;

    /**
     * @var int
     */
    private $education;

    /**
     * @var \Rencredit\Type\Employment
     */
    private $employment;

    /**
     * @var int
     */
    private $familyState;

    /**
     * @var int
     */
    private $incomeType;

    /**
     * @var \Rencredit\Type\Address
     */
    private $majorAddress;

    /**
     * @var string
     */
    private $motherLastName;

    /**
     * @var int
     */
    private $numberOfDependent;

    /**
     * @var int
     */
    private $pinDeliveryType;

    /**
     * @var int
     */
    private $preferedContactTimeFrom;

    /**
     * @var int
     */
    private $preferedContactTimeTo;

    /**
     * @var int
     */
    private $preferedContactType;

    /**
     * @var \Rencredit\Type\Property
     */
    private $property;

    /**
     * @var int
     */
    private $reportDeliveryAddress;

    /**
     * @var int
     */
    private $reportDeliveryType;

    /**
     * @var int
     */
    private $secondIncomeType;

    /**
     * @var float
     */
    private $secondMonthlyIncomeInRub;

    /**
     * @var int
     */
    private $sex;

    /**
     * @var \Rencredit\Type\Phone
     */
    private $mobilePhone;

    /**
     * @var \Rencredit\Type\Phone
     */
    private $homePhone;

    /**
     * @var \Rencredit\Type\Phone
     */
    private $workPhone;

    /**
     * @var string
     */
    private $email;

    /**
     * @var float
     */
    private $monthlyIncomeInRub;

    /**
     * @var \Rencredit\Type\ConsentFlag
     */
    private $consentFlags;

    /**
     * @var bool
     */
    private $gender;

    /**
     * @var int
     */
    private $maritalStatus;

    /**
     * @var int
     */
    private $dependantsNumber;

    /**
     * @var string
     */
    private $embasingName;

    /**
     * @var \Rencredit\Type\Passport
     */
    private $passport;

    /**
     * @var \Rencredit\Type\ContactType
     */
    private $contactType;

    /**
     * @var \Rencredit\Type\Address
     */
    private $registrationAddress;

    /**
     * @return \DateTimeInterface
     */
    public function getBirthDate(): \DateTimeInterface
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTimeInterface $birthDate
     */
    public function setBirthDate(\DateTimeInterface $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return string
     */
    public function getBirthPlace(): string
    {
        return $this->birthPlace;
    }

    /**
     * @param string $birthPlace
     */
    public function setBirthPlace(string $birthPlace): void
    {
        $this->birthPlace = $birthPlace;
    }

    /**
     * @return Address
     */
    public function getContactAddress(): Address
    {
        return $this->contactAddress;
    }

    /**
     * @param Address $contactAddress
     */
    public function setContactAddress(Address $contactAddress): void
    {
        $this->contactAddress = $contactAddress;
    }

    /**
     * @return string
     */
    public function getCreditHistorySubjectCode(): string
    {
        return $this->creditHistorySubjectCode;
    }

    /**
     * @param string $creditHistorySubjectCode
     */
    public function setCreditHistorySubjectCode(string $creditHistorySubjectCode): void
    {
        $this->creditHistorySubjectCode = $creditHistorySubjectCode;
    }

    /**
     * @return Passport
     */
    public function getDocument1(): Passport
    {
        return $this->document1;
    }

    /**
     * @param Passport $document1
     */
    public function setDocument1(Passport $document1): void
    {
        $this->document1 = $document1;
    }

    /**
     * @return Document
     */
    public function getDocument2(): Document
    {
        return $this->document2;
    }

    /**
     * @param Document $document2
     */
    public function setDocument2(Document $document2): void
    {
        $this->document2 = $document2;
    }

    /**
     * @return string
     */
    public function getSnils(): string
    {
        return $this->snils;
    }

    /**
     * @param string $snils
     */
    public function setSnils(string $snils): void
    {
        $this->snils = $snils;
    }

    /**
     * @return int
     */
    public function getEducation(): int
    {
        return $this->education;
    }

    /**
     * @param int $education
     */
    public function setEducation(int $education): void
    {
        $this->education = $education;
    }

    /**
     * @return Employment
     */
    public function getEmployment(): Employment
    {
        return $this->employment;
    }

    /**
     * @param Employment $employment
     */
    public function setEmployment(Employment $employment): void
    {
        $this->employment = $employment;
    }

    /**
     * @return int
     */
    public function getFamilyState(): int
    {
        return $this->familyState;
    }

    /**
     * @param int $familyState
     */
    public function setFamilyState(int $familyState): void
    {
        $this->familyState = $familyState;
    }

    /**
     * @return int
     */
    public function getIncomeType(): int
    {
        return $this->incomeType;
    }

    /**
     * @param int $incomeType
     */
    public function setIncomeType(int $incomeType): void
    {
        $this->incomeType = $incomeType;
    }

    /**
     * @return Address
     */
    public function getMajorAddress(): Address
    {
        return $this->majorAddress;
    }

    /**
     * @param Address $majorAddress
     */
    public function setMajorAddress(Address $majorAddress): void
    {
        $this->majorAddress = $majorAddress;
    }

    /**
     * @return string
     */
    public function getMotherLastName(): string
    {
        return $this->motherLastName;
    }

    /**
     * @param string $motherLastName
     */
    public function setMotherLastName(string $motherLastName): void
    {
        $this->motherLastName = $motherLastName;
    }

    /**
     * @return int
     */
    public function getNumberOfDependent(): int
    {
        return $this->numberOfDependent;
    }

    /**
     * @param int $numberOfDependent
     */
    public function setNumberOfDependent(int $numberOfDependent): void
    {
        $this->numberOfDependent = $numberOfDependent;
    }

    /**
     * @return int
     */
    public function getPinDeliveryType(): int
    {
        return $this->pinDeliveryType;
    }

    /**
     * @param int $pinDeliveryType
     */
    public function setPinDeliveryType(int $pinDeliveryType): void
    {
        $this->pinDeliveryType = $pinDeliveryType;
    }

    /**
     * @return int
     */
    public function getPreferedContactTimeFrom(): int
    {
        return $this->preferedContactTimeFrom;
    }

    /**
     * @param int $preferedContactTimeFrom
     */
    public function setPreferedContactTimeFrom(int $preferedContactTimeFrom): void
    {
        $this->preferedContactTimeFrom = $preferedContactTimeFrom;
    }

    /**
     * @return int
     */
    public function getPreferedContactTimeTo(): int
    {
        return $this->preferedContactTimeTo;
    }

    /**
     * @param int $preferedContactTimeTo
     */
    public function setPreferedContactTimeTo(int $preferedContactTimeTo): void
    {
        $this->preferedContactTimeTo = $preferedContactTimeTo;
    }

    /**
     * @return int
     */
    public function getPreferedContactType(): int
    {
        return $this->preferedContactType;
    }

    /**
     * @param int $preferedContactType
     */
    public function setPreferedContactType(int $preferedContactType): void
    {
        $this->preferedContactType = $preferedContactType;
    }

    /**
     * @return Property
     */
    public function getProperty(): Property
    {
        return $this->property;
    }

    /**
     * @param Property $property
     */
    public function setProperty(Property $property): void
    {
        $this->property = $property;
    }

    /**
     * @return int
     */
    public function getReportDeliveryAddress(): int
    {
        return $this->reportDeliveryAddress;
    }

    /**
     * @param int $reportDeliveryAddress
     */
    public function setReportDeliveryAddress(int $reportDeliveryAddress): void
    {
        $this->reportDeliveryAddress = $reportDeliveryAddress;
    }

    /**
     * @return int
     */
    public function getReportDeliveryType(): int
    {
        return $this->reportDeliveryType;
    }

    /**
     * @param int $reportDeliveryType
     */
    public function setReportDeliveryType(int $reportDeliveryType): void
    {
        $this->reportDeliveryType = $reportDeliveryType;
    }

    /**
     * @return int
     */
    public function getSecondIncomeType(): int
    {
        return $this->secondIncomeType;
    }

    /**
     * @param int $secondIncomeType
     */
    public function setSecondIncomeType(int $secondIncomeType): void
    {
        $this->secondIncomeType = $secondIncomeType;
    }

    /**
     * @return float
     */
    public function getSecondMonthlyIncomeInRub(): float
    {
        return $this->secondMonthlyIncomeInRub;
    }

    /**
     * @param float $secondMonthlyIncomeInRub
     */
    public function setSecondMonthlyIncomeInRub(float $secondMonthlyIncomeInRub): void
    {
        $this->secondMonthlyIncomeInRub = $secondMonthlyIncomeInRub;
    }

    /**
     * @return int
     */
    public function getSex(): int
    {
        return $this->sex;
    }

    /**
     * @param int $sex
     */
    public function setSex(int $sex): void
    {
        $this->sex = $sex;
    }

    /**
     * @return Phone
     */
    public function getMobilePhone(): Phone
    {
        return $this->mobilePhone;
    }

    /**
     * @param Phone $mobilePhone
     */
    public function setMobilePhone(Phone $mobilePhone): void
    {
        $this->mobilePhone = $mobilePhone;
    }

    /**
     * @return Phone
     */
    public function getHomePhone(): Phone
    {
        return $this->homePhone;
    }

    /**
     * @param Phone $homePhone
     */
    public function setHomePhone(Phone $homePhone): void
    {
        $this->homePhone = $homePhone;
    }

    /**
     * @return Phone
     */
    public function getWorkPhone(): Phone
    {
        return $this->workPhone;
    }

    /**
     * @param Phone $workPhone
     */
    public function setWorkPhone(Phone $workPhone): void
    {
        $this->workPhone = $workPhone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return float
     */
    public function getMonthlyIncomeInRub(): float
    {
        return $this->monthlyIncomeInRub;
    }

    /**
     * @param float $monthlyIncomeInRub
     */
    public function setMonthlyIncomeInRub(float $monthlyIncomeInRub): void
    {
        $this->monthlyIncomeInRub = $monthlyIncomeInRub;
    }

    /**
     * @return ConsentFlag
     */
    public function getConsentFlags(): ConsentFlag
    {
        return $this->consentFlags;
    }

    /**
     * @param ConsentFlag $consentFlags
     */
    public function setConsentFlags(ConsentFlag $consentFlags): void
    {
        $this->consentFlags = $consentFlags;
    }

    /**
     * @return bool
     */
    public function isGender(): bool
    {
        return $this->gender;
    }

    /**
     * @param bool $gender
     */
    public function setGender(bool $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return int
     */
    public function getMaritalStatus(): int
    {
        return $this->maritalStatus;
    }

    /**
     * @param int $maritalStatus
     */
    public function setMaritalStatus(int $maritalStatus): void
    {
        $this->maritalStatus = $maritalStatus;
    }

    /**
     * @return int
     */
    public function getDependantsNumber(): int
    {
        return $this->dependantsNumber;
    }

    /**
     * @param int $dependantsNumber
     */
    public function setDependantsNumber(int $dependantsNumber): void
    {
        $this->dependantsNumber = $dependantsNumber;
    }

    /**
     * @return string
     */
    public function getEmbasingName(): string
    {
        return $this->embasingName;
    }

    /**
     * @param string $embasingName
     */
    public function setEmbasingName(string $embasingName): void
    {
        $this->embasingName = $embasingName;
    }

    /**
     * @return Passport
     */
    public function getPassport(): Passport
    {
        return $this->passport;
    }

    /**
     * @param Passport $passport
     */
    public function setPassport(Passport $passport): void
    {
        $this->passport = $passport;
    }

    /**
     * @return ContactType
     */
    public function getContactType(): ContactType
    {
        return $this->contactType;
    }

    /**
     * @param ContactType $contactType
     */
    public function setContactType(ContactType $contactType): void
    {
        $this->contactType = $contactType;
    }

    /**
     * @return Address
     */
    public function getRegistrationAddress(): Address
    {
        return $this->registrationAddress;
    }

    /**
     * @param Address $registrationAddress
     */
    public function setRegistrationAddress(Address $registrationAddress): void
    {
        $this->registrationAddress = $registrationAddress;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getPatronymic(): string
    {
        return $this->patronymic;
    }

    /**
     * @param string $patronymic
     */
    public function setPatronymic(string $patronymic): void
    {
        $this->patronymic = $patronymic;
    }

}

