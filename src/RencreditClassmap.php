<?php

namespace Rencredit;

use Rencredit\Type;
use Phpro\SoapClient\Soap\ClassMap\ClassMapCollection;
use Phpro\SoapClient\Soap\ClassMap\ClassMap;

class RencreditClassmap
{

    public static function getCollection() : \Phpro\SoapClient\Soap\ClassMap\ClassMapCollection
    {
        return new ClassMapCollection([
            new ClassMap('proofIncomeRequest', Type\ProofIncomeRequest::class),
            new ClassMap('proofIncomeResponse', Type\ProofIncomeResponse::class),
            new ClassMap('sendScansDocumentsRequest', Type\SendScansDocumentsRequest::class),
            new ClassMap('IncomeDocScan', Type\IncomeDocScan::class),
            new ClassMap('docScans', Type\DocScans::class),
            new ClassMap('sendScansDocumentsResponse', Type\SendScansDocumentsResponse::class),
            new ClassMap('scansResponse', Type\ScansResponse::class),
            new ClassMap('scoreRequest', Type\ScoreRequest::class),
            new ClassMap('scoreResponse', Type\ScoreResponse::class),
            new ClassMap('additionalScoreRequest', Type\AdditionalScoreRequest::class),
            new ClassMap('additionalScoreResponse', Type\AdditionalScoreResponse::class),
            new ClassMap('iScoreRequest', Type\IScoreRequest::class),
            new ClassMap('iScoreResponse', Type\IScoreResponse::class),
            new ClassMap('basicForm', Type\BasicForm::class),
            new ClassMap('form', Type\Form::class),
            new ClassMap('iForm', Type\IForm::class),
            new ClassMap('insurance', Type\Insurance::class),
            new ClassMap('person', Type\Person::class),
            new ClassMap('contactPerson', Type\ContactPerson::class),
            new ClassMap('iBorrower', Type\IBorrower::class),
            new ClassMap('borrower', Type\Borrower::class),
            new ClassMap('address', Type\Address::class),
            new ClassMap('document', Type\Document::class),
            new ClassMap('passport', Type\Passport::class),
            new ClassMap('employment', Type\Employment::class),
            new ClassMap('phone', Type\Phone::class),
            new ClassMap('property', Type\Property::class),
            new ClassMap('car', Type\Car::class),
            new ClassMap('goods', Type\Goods::class),
            new ClassMap('scan', Type\Scan::class),
            new ClassMap('offer', Type\Offer::class),
            new ClassMap('downPayment', Type\DownPayment::class),
            new ClassMap('creditPeriod', Type\CreditPeriod::class),
            new ClassMap('checkScoreStatusRequest', Type\CheckScoreStatusRequest::class),
            new ClassMap('checkScoreStatusResponse', Type\CheckScoreStatusResponse::class),
            new ClassMap('generateDocsResult', Type\GenerateDocsResult::class),
            new ClassMap('status', Type\Status::class),
            new ClassMap('confirmOfferRequest', Type\ConfirmOfferRequest::class),
            new ClassMap('confirmOfferResponse', Type\ConfirmOfferResponse::class),
            new ClassMap('generateDocsRequest', Type\GenerateDocsRequest::class),
            new ClassMap('generateDocsResponse', Type\GenerateDocsResponse::class),
            new ClassMap('creditInfo', Type\CreditInfo::class),
            new ClassMap('checkStatusScansRequest', Type\CheckStatusScansRequest::class),
            new ClassMap('checkStatusScansResponse', Type\CheckStatusScansResponse::class),
            new ClassMap('cancelRequest', Type\CancelRequest::class),
            new ClassMap('cancelResponse', Type\CancelResponse::class),
            new ClassMap('productInfoRequest', Type\ProductInfoRequest::class),
            new ClassMap('productInfoResponse', Type\ProductInfoResponse::class),
            new ClassMap('productInformation', Type\ProductInformation::class),
            new ClassMap('product', Type\Product::class),
            new ClassMap('infoServicePackage', Type\InfoServicePackage::class),
            new ClassMap('infoInsurance', Type\InfoInsurance::class),
            new ClassMap('confirmShipmentRequest', Type\ConfirmShipmentRequest::class),
            new ClassMap('confirmShipmentResponse', Type\ConfirmShipmentResponse::class),
            new ClassMap('transferForResigningRequest', Type\TransferForResigningRequest::class),
            new ClassMap('transferForResigningResponse', Type\TransferForResigningResponse::class),
            new ClassMap('resendSmsRequest', Type\ResendSmsRequest::class),
            new ClassMap('resendSmsResponse', Type\ResendSmsResponse::class),
            new ClassMap('scanStatusesType', Type\ScanStatusesType::class),
            new ClassMap('scansStatus', Type\ScansStatus::class),
            new ClassMap('infoOffer', Type\InfoOffer::class),
            new ClassMap('servicePackages', Type\ServicePackages::class),
            new ClassMap('ScanInfo', Type\ScanInfo::class),
            new ClassMap('LoanException', Type\LoanException::class),
            new ClassMap('ContactPersonFF', Type\ContactPersonFF::class),
            new ClassMap('StationaryPhone', Type\StationaryPhone::class),
            new ClassMap('MobilePhone', Type\MobilePhone::class),
            new ClassMap('BinaryFile', Type\BinaryFile::class),
            new ClassMap('HomeAddress', Type\HomeAddress::class),
            new ClassMap('IdDocument', Type\IdDocument::class),
            new ClassMap('ContactType', Type\ContactType::class),
            new ClassMap('ContactTypeFF', Type\ContactTypeFF::class),
            new ClassMap('Belongings', Type\Belongings::class),
            new ClassMap('Chattels', Type\Chattels::class),
            new ClassMap('RealEstate', Type\RealEstate::class),
            new ClassMap('BelongingsParameters', Type\BelongingsParameters::class),
            new ClassMap('ConsentFlag', Type\ConsentFlag::class),
        ]);
    }


}

