<?php

namespace Rencredit;

use Rencredit\Type;
use Phpro\SoapClient\Type\RequestInterface;
use Phpro\SoapClient\Type\ResultInterface;
use Phpro\SoapClient\Exception\SoapException;

class RencreditClient extends \Phpro\SoapClient\Client
{

    /**
     * @param RequestInterface|Type\ScoreRequest $parameters
     * @return ResultInterface|Type\ScoreResponse
     * @throws SoapException
     */
    public function score(\Rencredit\Type\ScoreRequest $parameters) : \Rencredit\Type\ScoreResponse
    {
        return $this->call('score', $parameters);
    }

    /**
     * @param RequestInterface|Type\AdditionalScoreRequest $parameters
     * @return ResultInterface|Type\AdditionalScoreResponse
     * @throws SoapException
     */
    public function additionalScore(\Rencredit\Type\AdditionalScoreRequest $parameters) : \Rencredit\Type\AdditionalScoreResponse
    {
        return $this->call('additionalScore', $parameters);
    }

    /**
     * @param RequestInterface|Type\IScoreRequest $parameters
     * @return ResultInterface|Type\IScoreResponse
     * @throws SoapException
     */
    public function iScore(\Rencredit\Type\IScoreRequest $parameters) : \Rencredit\Type\IScoreResponse
    {
        return $this->call('iScore', $parameters);
    }

    /**
     * @param RequestInterface|Type\CheckScoreStatusRequest $parameters
     * @return ResultInterface|Type\CheckScoreStatusResponse
     * @throws SoapException
     */
    public function checkScoreStatus(\Rencredit\Type\CheckScoreStatusRequest $parameters) : \Rencredit\Type\CheckScoreStatusResponse
    {
        return $this->call('checkScoreStatus', $parameters);
    }

    /**
     * @param RequestInterface|Type\ConfirmOfferRequest $parameters
     * @return ResultInterface|Type\ConfirmOfferResponse
     * @throws SoapException
     */
    public function confirmOffer(\Rencredit\Type\ConfirmOfferRequest $parameters) : \Rencredit\Type\ConfirmOfferResponse
    {
        return $this->call('confirmOffer', $parameters);
    }

    /**
     * @param RequestInterface|Type\GenerateDocsRequest $parameters
     * @return ResultInterface|Type\GenerateDocsResponse
     * @throws SoapException
     */
    public function generateDocs(\Rencredit\Type\GenerateDocsRequest $parameters) : \Rencredit\Type\GenerateDocsResponse
    {
        return $this->call('generateDocs', $parameters);
    }

    /**
     * @param RequestInterface|Type\SendScansDocumentsRequest $parameters
     * @return ResultInterface|Type\SendScansDocumentsResponse
     * @throws SoapException
     */
    public function sendScansDocuments(\Rencredit\Type\SendScansDocumentsRequest $parameters) : \Rencredit\Type\SendScansDocumentsResponse
    {
        return $this->call('sendScansDocuments', $parameters);
    }

    /**
     * @param RequestInterface|Type\CheckStatusScansRequest $parameters
     * @return ResultInterface|Type\CheckStatusScansResponse
     * @throws SoapException
     */
    public function checkStatusScans(\Rencredit\Type\CheckStatusScansRequest $parameters) : \Rencredit\Type\CheckStatusScansResponse
    {
        return $this->call('checkStatusScans', $parameters);
    }

    /**
     * @param RequestInterface|Type\CancelRequest $parameters
     * @return ResultInterface|Type\CancelResponse
     * @throws SoapException
     */
    public function cancel(\Rencredit\Type\CancelRequest $parameters) : \Rencredit\Type\CancelResponse
    {
        return $this->call('cancel', $parameters);
    }

    /**
     * @param RequestInterface|Type\ProofIncomeRequest $parameters
     * @return ResultInterface|Type\ProofIncomeResponse
     * @throws SoapException
     */
    public function proofIncome(\Rencredit\Type\ProofIncomeRequest $parameters) : \Rencredit\Type\ProofIncomeResponse
    {
        return $this->call('proofIncome', $parameters);
    }

    /**
     * @param RequestInterface|Type\ProductInfoRequest $parameters
     * @return ResultInterface|Type\ProductInfoResponse
     * @throws SoapException
     */
    public function productInfo(\Rencredit\Type\ProductInfoRequest $parameters) : \Rencredit\Type\ProductInfoResponse
    {
        return $this->call('productInfo', $parameters);
    }

    /**
     * @param RequestInterface|Type\ConfirmShipmentRequest $parameters
     * @return ResultInterface|Type\ConfirmShipmentResponse
     * @throws SoapException
     */
    public function confirmShipment(\Rencredit\Type\ConfirmShipmentRequest $parameters) : \Rencredit\Type\ConfirmShipmentResponse
    {
        return $this->call('confirmShipment', $parameters);
    }

    /**
     * @param RequestInterface|Type\TransferForResigningRequest $parameters
     * @return ResultInterface|Type\TransferForResigningResponse
     * @throws SoapException
     */
    public function transferForResigning(\Rencredit\Type\TransferForResigningRequest $parameters) : \Rencredit\Type\TransferForResigningResponse
    {
        return $this->call('transferForResigning', $parameters);
    }

    /**
     * @param RequestInterface|Type\ResendSmsRequest $parameters
     * @return ResultInterface|Type\ResendSmsResponse
     * @throws SoapException
     */
    public function resendSms(\Rencredit\Type\ResendSmsRequest $parameters) : \Rencredit\Type\ResendSmsResponse
    {
        return $this->call('resendSms', $parameters);
    }


}

