<?php

namespace Rencredit;

use Phpro\SoapClient\Wsdl\Provider\LocalWsdlProvider;
use Rencredit\custom\MyDateTime;
use Rencredit\Type\Address;
use Rencredit\Type\Borrower;
use Rencredit\Type\Car;
use Rencredit\Type\CheckScoreStatusRequest;
use Rencredit\Type\ConfirmOfferRequest;
use Rencredit\Type\ContactPerson;
use Rencredit\Type\CreditPeriod;
use Rencredit\Type\DownPayment;
use Rencredit\Type\Employment;
use Rencredit\Type\Form;
use Rencredit\Type\Goods;
use Rencredit\Type\Offer;
use Rencredit\Type\Passport;
use Rencredit\Type\Phone;
use Rencredit\Type\Property;
use Rencredit\Type\Scan;
use Rencredit\Type\ScoreRequest;

class Container
{

    public static function call()
    {
        // https://github.com/phpro/soap-client/blob/master/docs/usage.md
        $provider = LocalWsdlProvider::create();
        // $wsdl = 'https://fastloanpreprodft.rencredit.ru/SecLoanServicePilot/SecLoanCrbOnlineSrv?wsdl';
        $wsdl = $provider->provide('./resources/SecLoanCrbOnlineSrv.wsdl');
        $client = RencreditClientFactory::factory($wsdl);
//        self::callCheckScoreStatus($client);
//        self::callConfirmOrder($client);
        self::callScore($client);
    }

    public static function callScore(RencreditClient $client): string
    {
        $application = new Form();
        $application->setPosId(103);
        $application->setUserLogin("ext_dtikhonov2");
        $application->setPartnerID(123);
        $application->setPartnerNetId(4);
        $application->setCreditPeriodInMonths(24);
        $application->setInitialPayment(39);
        $application->setCreditProductId("RCCF13_prem17");
        $borrower = new Borrower();
        $borrower->setFirstName("ТЕСТОВЫЙ");
        $borrower->setLastName("АРТЕМ");
        $borrower->setPatronymic("ВИКТОРОВИЧ");
        // 1963.01.01 09:38:03
        $borrower->setBirthDate(new MyDateTime("1963-01-01"));
        $borrower->setBirthPlace("К|а|з|а|н|ь");
        $borrower->setContactAddress(self::createAddress());
        // 2012-11-19 12:00:00
        $borrower->setDocument1(new Passport("731369", "4269", 1, new MyDateTime("2012-11-19"), "Казанским РО ГОВД УВД", "123-123"));
        $borrower->setEducation(1);
        $employment = new Employment();
        $employment->setActivitiesType(5);
        $employment->setEmployeeFrom("1994-12-12 12:00:00");
        $employment->setEmployerAddress(self::createAddress());
        $employment->setEmployerName("Кирилл");
        $employment->setEmployerPhone(new Phone("1", "15", "926", "1231233", "Коля"));
        $employment->setOccupation("Профессор");
        $employment->setOccupationType(1);
        $borrower->setEmployment($employment);
        $borrower->setFamilyState(1);
        $borrower->setIncomeType(1);
        $borrower->setMajorAddress(self::createAddress());
        $borrower->setMotherLastName("Останкинская");
        $borrower->setNumberOfDependent(1);
        $borrower->setPinDeliveryType(1);
        $borrower->setProperty(new Property(new Car("2015", "ВАЗ", "ф485фы55")));
        $borrower->setReportDeliveryAddress(1);
        $borrower->setReportDeliveryType(1);
        $borrower->setIncomeType(1);
        $borrower->setSecondMonthlyIncomeInRub(1000);
        $borrower->setSex(1);
        $borrower->setMobilePhone(new Phone("1", "15", "926", "1231233", "Коля"));
        $borrower->setHomePhone(new Phone("1", "15", "926", "1231233", "Коля"));
        $borrower->setEmail("ext_dtikhonov2@rencredit.ru");
        $borrower->setMonthlyIncomeInRub(10000);
        $application->setBorrower($borrower);
        $contactPerson1 = new ContactPerson();
        $contactPerson1->setFirstName("Кирилл");
        $contactPerson1->setLastName("Кирилл");
        $contactPerson1->setPatronymic("Кириллович");
        $contactPerson1->setMobilePhone(new Phone("1", "15", "926", "1231233", "Коля"));
        $contactPerson1->setHomePhone(new Phone("1", "15", "926", "1231233", "Коля"));
        $contactPerson1->setWorkPhone(new Phone("1", "15", "926", "1231233", "Коля"));
        $contactPerson1->setEmail("ext_dtikhonov2@rencredit.ru");
        $contactPerson1->setMonthlyIncomeInRub(10000);
        $application->setContactPerson1($contactPerson1);
        $goods = new Goods();
        $goods->setCategory(1);
        $goods->setGroup(1);
        $goods->setName("Звездолёт");
        $goods->setUnitPriceInRub(65001);
        $goods->setUnitsNumber(1);
        $application->setGoods($goods);
        $application->setBorrowerDocumentsScans(new Scan(1000, "STUBSCANDATA"));
        $application->setTransferClaimDenied(true);
        $request = new ScoreRequest($application);
        $response = $client->score($request);
        echo $response->getCorrelationId();
        return $response->getCorrelationId();
    }

    public static function callConfirmOrder(RencreditClient $client): string
    {
        $offer = new Offer();
        $offer = $offer->withOfferId(1);
        $offer = $offer->withCreditLimit(12325.35);
        $offer = $offer->withPercentageRate(85.0);
        $downPayment = new DownPayment();
        $downPayment = $downPayment->withDownPayment(9910.0);
        $offer = $offer->withDownPayments($downPayment);
        $creditPeriod = new CreditPeriod();
        $creditPeriod = $creditPeriod->withCreditPeriod(10);
        $offer = $offer->withCreditPeriods($creditPeriod);
        $offer = $offer->withProductName('RCCF13_EXP_KR');
        $response = $client->confirmOffer(new ConfirmOfferRequest(
            'CRB2016090311401906',
            $offer));
        return $response->getReturn() ? 'true' : 'false';;
    }

    public static function callCheckScoreStatus(RencreditClient $client): string
    {
        $correlationId = "CRB2020020821592338";
        $response = $client->checkScoreStatus(new CheckScoreStatusRequest($correlationId));
        echo $response->getReturn()->getCode();
        echo $response->getReturn()->getGoodsSumPrice();
        return $response->getReturn()->getCode();
    }

    public static function createAddress(): Address
    {
        $address = new Address();
        $address->setApartmentNumber("1");
        $address->setHouse("1");
        $address->setBuilding("2");
        $address->setDistrict("Пушкинский");
        $address->setStructure(5);
        $address->setPlaceType(2);
        $address->setPlaceName("Суворовск");
        $address->setPropertyStatus(1);
        $address->setRegion(2);
        $address->setStreet("Останкинская");
        $address->setStreetType(103);
        $address->setTimeOfLivingInMonths(12);
        $address->setTown("Казань");
        $address->setTownType(72);
        $address->setZipCode("123456");
        return $address;
    }
}
