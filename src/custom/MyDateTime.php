<?php


namespace Rencredit\custom;


class MyDateTime extends \DateTimeImmutable
{
    /**
     * MyDateTime constructor.
     * @param string $time
     * <p>A date/time string. Valid formats are explained in {@link https://secure.php.net/manual/en/datetime.formats.php Date and Time Formats}.</p>
     * <p>
     * @throws \Exception
     */
    public function __construct($time)
    {
        parent::__construct($time);
    }

    public function __toString(): string
    {
        return $this->format('Y-m-d H:i:s');
    }
}